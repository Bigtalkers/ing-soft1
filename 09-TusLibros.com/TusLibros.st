!classDefinition: #CarritoTest category: #TusLibros!
TestCase subclass: #CarritoTest
	instanceVariableNames: 'libro catalogo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/16/2018 13:46:58'!
setUp
	libro _ '817525766-0'.
	catalogo _ Catalogo new.
	catalogo add: libro.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/7/2018 15:06:16'!
test01CarritoShouldStartEmpty
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	self assert: carrito isEmpty.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/16/2018 14:06:30'!
test02CarritoShouldAllowAddLibro
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	carrito add: libro.
	self deny: carrito isEmpty.
	self assert: [carrito includes: libro].! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/7/2018 15:06:34'!
test03CarritoShouldAllowRemoveLibro
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	carrito add: libro.
	carrito remove: libro.
	self assert: carrito isEmpty.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/7/2018 15:07:34'!
test04CarritoShouldNotAllowRemoveNotIncluded
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	self
		should: [ carrito remove: libro ]
		raise: Error
		withExceptionDo: [ :error |
			self assert: error messageText = Carrito errorLibroNoEnCarrito ].! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/7/2018 15:08:40'!
test05CarritoShouldListTheAddedLibros
	| carrito libros |
	carrito _ Carrito withCatalog: catalogo.
	carrito add: libro.
	libros _ OrderedCollection new.
	libros add: libro.
	self
		assert: libros
		equals: carrito list.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/16/2018 14:09:25'!
test06CarritoShouldAllowAddingMultipleCopies
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	carrito add: libro amount: 2.
	self deny: carrito isEmpty.
	self assert: (carrito occurrencesOf: libro) equals: 2.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/16/2018 14:18:22'!
test07CarritoShouldNotAllowAddingLibrosNotInCatalogo
	| carrito libro catalogo |
	libro _ '817525766-0'.
	catalogo _ Catalogo new.
	carrito _ Carrito withCatalog: catalogo.
	self should: [carrito add: libro amount: 2.] raise: Error 
	withExceptionDo: [:error | 
		self assert: error messageText = Carrito errorNoExisteTalLibro.
		self assert: carrito isEmpty].! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/16/2018 14:23:27'!
test08CarritoShouldNotAllowAddingNonNaturalCopies
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	self should: [carrito add: libro amount: 0.] raise: Error withExceptionDo: [ :error |
		self assert: error messageText = Carrito errorNoSePuedenAgregarCopiasNoNaturales.
		self assert: carrito isEmpty.
		].
	self should: [carrito add: libro amount: 1/2.] raise: Error withExceptionDo: [ :error |
		self assert: error messageText = Carrito errorNoSePuedenAgregarCopiasNoNaturales.
		self assert: carrito isEmpty.
		].! !


!classDefinition: #Carrito category: #TusLibros!
Object subclass: #Carrito
	instanceVariableNames: 'libros catalogo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Carrito methodsFor: 'testing' stamp: 'JOP 6/6/2018 22:50:10'!
isEmpty
	^ libros isEmpty! !


!Carrito methodsFor: 'modifying' stamp: 'JOP 6/7/2018 15:05:26'!
add: aLibro 
	(catalogo includes: aLibro) ifFalse: [self error: Catalogo errorNoExisteTalLibro].
	libros add: aLibro! !

!Carrito methodsFor: 'modifying' stamp: 'JOP 6/16/2018 14:22:47'!
add: aLibro amount: anAmount
	(catalogo includes: aLibro) ifFalse: [self error: Carrito errorNoExisteTalLibro].
	(anAmount > 0 and: anAmount isInteger) ifFalse: [self error: Carrito errorNoSePuedenAgregarCopiasNoNaturales].
	libros add: aLibro withOccurrences: anAmount.! !

!Carrito methodsFor: 'modifying' stamp: 'JOP 6/6/2018 23:39:33'!
remove: aLibro 
	libros remove: aLibro ifAbsent: [self error: self class errorLibroNoEnCarrito]! !


!Carrito methodsFor: 'initialization' stamp: 'JOP 6/7/2018 14:34:02'!
initializeWith: aCatalog
	libros _ OrderedCollection new.
	catalogo _ aCatalog.! !


!Carrito methodsFor: 'accessing' stamp: 'JOP 6/16/2018 14:01:59'!
includes: aLibro 
	^ libros includes: aLibro! !

!Carrito methodsFor: 'accessing' stamp: 'SCN 6/7/2018 02:11:08'!
list
	^libros copy.! !

!Carrito methodsFor: 'accessing' stamp: 'JOP 6/16/2018 14:09:55'!
occurrencesOf: aLibro 
	^ libros occurrencesOf: aLibro! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Carrito class' category: #TusLibros!
Carrito class
	instanceVariableNames: ''!

!Carrito class methodsFor: 'errors' stamp: 'JOP 6/6/2018 23:30:56'!
errorLibroNoEnCarrito
	^ 'No existe tal libro en el carrito'! !

!Carrito class methodsFor: 'errors' stamp: 'JOP 6/16/2018 14:16:16'!
errorNoExisteTalLibro
	^ 'No existe tal libro en el cat�logo'! !

!Carrito class methodsFor: 'errors' stamp: 'JOP 6/16/2018 14:21:44'!
errorNoSePuedenAgregarCopiasNoNaturales
	^ 'No se puede agregar una cantidad de copias no natural'! !


!Carrito class methodsFor: 'instance creation' stamp: 'JOP 6/7/2018 14:33:37'!
withCatalog: aCatalog
	^ self new initializeWith: aCatalog.! !


!classDefinition: #Catalogo category: #TusLibros!
Object subclass: #Catalogo
	instanceVariableNames: 'listaDeLibros'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Catalogo methodsFor: 'initialization' stamp: 'JOP 6/7/2018 14:57:54'!
initialize
	listaDeLibros _ Set new.! !


!Catalogo methodsFor: 'testing' stamp: 'JOP 6/7/2018 14:48:43'!
includes: aLibro
	^ listaDeLibros includes: aLibro! !


!Catalogo methodsFor: 'modifying' stamp: 'JOP 6/7/2018 15:01:47'!
add: aLibro 
	listaDeLibros add: aLibro! !
