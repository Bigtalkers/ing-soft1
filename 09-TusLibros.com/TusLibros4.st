!classDefinition: #CarritoTest category: #TusLibros!
TestCase subclass: #CarritoTest
	instanceVariableNames: 'libro catalogo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CarritoTest methodsFor: 'testing setup' stamp: 'JOP 6/18/2018 08:14:01'!
setUp
	libro _ '817525766-0'.
	catalogo _ Set new.
	catalogo add: libro.! !


!CarritoTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 08:14:32'!
test01CarritoShouldStartEmpty
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	self assert: carrito isEmpty.! !

!CarritoTest methodsFor: 'tests' stamp: 'SCN 6/18/2018 14:30:05'!
test02CarritoShouldAllowAddingLibro
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	carrito add: libro.
	self deny: carrito isEmpty.
	self assert: [ carrito includes: libro ].! !

!CarritoTest methodsFor: 'tests' stamp: 'SCN 6/18/2018 14:32:22'!
test03CarritoShouldAllowRemovingLibro
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	carrito add: libro.
	carrito remove: libro.
	self assert: carrito isEmpty.! !

!CarritoTest methodsFor: 'tests' stamp: 'SCN 6/18/2018 14:32:57'!
test04CarritoShouldNotAllowRemovingNotIncludedLibro
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	self
		should: [ carrito remove: libro ]
		raise: Error
		withExceptionDo: [ :error |
			self assert: error messageText = Carrito errorLibroNoEnCarrito ].! !

!CarritoTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 08:14:44'!
test05CarritoShouldListTheAddedLibros
	| carrito libros |
	carrito _ Carrito withCatalog: catalogo.
	carrito add: libro.
	libros _ OrderedCollection new.
	libros add: libro.
	self
		assert: libros
		equals: carrito list.! !

!CarritoTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 08:14:46'!
test06CarritoShouldAllowAddingMultipleCopies
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	carrito
		add: libro
		amount: 2.
	self deny: carrito isEmpty.
	self
		assert: (carrito occurrencesOf: libro)
		equals: 2.! !

!CarritoTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 08:52:00'!
test07CarritoShouldNotAllowAddingLibrosNotInCatalogo
	| carrito libro catalogo |
	libro _ '817525766-0'.
	catalogo _ OrderedCollection new.
	carrito _ Carrito withCatalog: catalogo.
	self
		should: [
			carrito
				add: libro
				amount: 2 ]
		raise: Error
		withExceptionDo: [ :error |
			self assert: error messageText = Carrito errorNoExisteTalLibro.
			self assert: carrito isEmpty ].! !

!CarritoTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 08:14:51'!
test08CarritoShouldNotAllowAddingNonNaturalCopies
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	self
		should: [
			carrito
				add: libro
				amount: 0 ]
		raise: Error
		withExceptionDo: [ :error |
			self assert: error messageText = Carrito errorNoSePuedenAgregarCopiasNoNaturales.
			self assert: carrito isEmpty ].
	self
		should: [
			carrito
				add: libro
				amount: 1 / 2 ]
		raise: Error
		withExceptionDo: [ :error |
			self assert: error messageText = Carrito errorNoSePuedenAgregarCopiasNoNaturales.
			self assert: carrito isEmpty ].! !

!CarritoTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 09:06:02'!
test09CarritoShouldInjectIntoContentsCorrectly
	| carrito libros |
	carrito _ Carrito withCatalog: catalogo.
	carrito add: libro.
	libros _ OrderedCollection new.
	carrito inject: libros into: [:acum :elem | acum add: elem].
	self assert: libros size = 1.
	self assert: (libros includes: libro).! !


!classDefinition: #CashierTest category: #TusLibros!
TestCase subclass: #CashierTest
	instanceVariableNames: 'precios catalogo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'testing setup' stamp: 'JOP 6/18/2018 08:30:28'!
setUp
	precios _ Dictionary new 
		at: 'Fundación' put: 50;	
		at: 'Fundación e Imperio' put: 100;
		at: 'Segunda Fundación' put: 200; yourself.
	catalogo _ precios keys.! !


!CashierTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 11:17:36'!
test01ShouldNotAllowEmptyCheckout
	| cashier carrito card |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	card _ CreditCard
		of: 'Grounded Time Traveler'
		withNumber: '2'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
	self
		should: [
			cashier
				checkout: carrito
				withCreditCard: card 
				withProcessor: PaymentProcessor new]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :error |
			self assert: error messageText = Cashier errorCheckoutDeCarritoVacio.
			self assert: cashier isEmpty].! !

!CashierTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 11:17:10'!
test02ShouldNotAllowExpiredCard
	| cashier carrito card |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	carrito add: 'Fundación'.
	card _ CreditCard
		of: 'Clueless Time Traveler'
		withNumber: '1'
		andExpiration: (GregorianMonthOfYear januaryOf: GregorianYear current).
	self
		should: [
			cashier
				checkout: carrito
				withCreditCard: card 
				withProcessor: PaymentProcessor new]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :error |
			self assert: error messageText = Cashier errorTarjetaVencida.
			self assert: cashier isEmpty].! !

!CashierTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 11:16:35'!
test03ShouldCalculateTotalCorrectly
	| cashier carrito card total |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	carrito
		add: 'Fundación'
		amount: 2;
		add: 'Fundación e Imperio'; 
		yourself.
	card _ CreditCard
		of: 'Grounded Time Traveler'
		withNumber: '2'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
	total _ cashier checkout: carrito withCreditCard: card withProcessor: PaymentProcessor new.
	self assert: 200 = total.! !

!CashierTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 11:17:49'!
test04ShouldIncludeSalesCorrectly
	| cashier carrito card |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	carrito
		add: 'Fundación'
		amount: 2;
		add: 'Fundación e Imperio'; 
		yourself.
	card _ CreditCard
		of: 'Grounded Time Traveler'
		withNumber: '2'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
	cashier checkout: carrito withCreditCard: card withProcessor: PaymentProcessor new.
	self assert: (cashier includes: (Sale of: carrito list total: 200)).! !

!CashierTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 11:17:59'!
test05ShouldListSalesCorrectly
	| cashier carrito card |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	carrito
		add: 'Fundación'
		amount: 2;
		add: 'Fundación e Imperio'; 
		yourself.
	card _ CreditCard
		of: 'Grounded Time Traveler'
		withNumber: '2'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
	cashier checkout: carrito withCreditCard: card withProcessor: PaymentProcessor new.
	self assert: (cashier list hasEqualElements: (Array with: (Sale of: carrito list total: 200))).! !

!CashierTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 11:18:08'!
test06ShouldAllowProcessorForCheckout
	| cashier carrito card processor |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	processor _ PaymentProcessor new.
	carrito
		add: 'Fundación'
		amount: 2;
		add: 'Fundación e Imperio'; 
		yourself.
	card _ CreditCard
		of: 'Grounded Time Traveler'
		withNumber: '2'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
	cashier checkout: carrito withCreditCard: card withProcessor: processor.
	self assert: (cashier list hasEqualElements: (Array with: (Sale of: carrito list total: 200))).! !

!CashierTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 11:22:14'!
test07ShouldNotAllowCheckoutWithoutEnoughFunds
	| cashier carrito card processor |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	processor _ PaymentProcessor new.
	carrito
		add: 'Fundación'
		amount: 2;
		add: 'Fundación e Imperio'; 
		yourself.
	card _ CreditCard
		of: 'Malos Fondos'
		withNumber: '3'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
	self should: [cashier checkout: carrito withCreditCard: card withProcessor: processor.] 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :error | 
			self assert: error messageText = PaymentProcessor errorNotEnoughFunds.
			self assert: cashier isEmpty.
			]! !


!classDefinition: #FachadaTest category: #TusLibros!
TestCase subclass: #FachadaTest
	instanceVariableNames: 'libro fachada card precios'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!FachadaTest methodsFor: 'tests' stamp: 'SCN 6/18/2018 14:27:58'!
test01ShouldAllowCreatingCarritoWithValidUser
	fachada
		register: 'Pepe'
		password: '1234'.
	fachada
		createCarritoUser: 'Pepe'
		password: '1234'.
	self assert: fachada totalCarritos > 0.! !

!FachadaTest methodsFor: 'tests' stamp: 'SCN 6/18/2018 14:28:23'!
test02ShouldNotAllowCreatingCarritoWithInvalidUser
	fachada
		register: 'Pepe'
		password: '1234'.
	self
		should: [
			fachada
				createCarritoUser: 'Roberto'
				password: '1234' ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :error |
			self assert: error messageText = Fachada errorNoExisteTalUser.
			self assert: fachada totalCarritos = 0 ].! !

!FachadaTest methodsFor: 'tests' stamp: 'SCN 6/18/2018 14:28:38'!
test03ShouldNotAllowCreatingCarritoWithInvalidPassword
	fachada
		register: 'Pepe'
		password: '1234'.
	self
		should: [
			fachada
				createCarritoUser: 'Pepe'
				password: '1235' ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :error |
			self assert: error messageText = Fachada errorContraseñaInvalida.
			self assert: fachada totalCarritos = 0 ].! !

!FachadaTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 12:59:37'!
test04ShouldAllowListingACarrito
	| carrito |
	fachada register: 'Pepe' password: '1234'.
	carrito _ fachada createCarritoUser: 'Pepe' password: '1234'.
	self assert: (fachada listCarrito: carrito) isEmpty.
	! !

!FachadaTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 12:52:48'!
test05ShouldAllowAddingStuffToCarrito
	| carrito list |
	fachada register: 'Pepe' password: '1234'.
	carrito _ fachada createCarritoUser: 'Pepe' password: '1234'.
	fachada add: libro amount: 2 toCarrito: carrito.
	list _ fachada listCarrito: carrito.
	self assert: (list occurrencesOf: libro) = 2.
	! !

!FachadaTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 14:57:49'!
test06ShouldAllowCheckoutOfCarrito
	| carrito |
	fachada register: 'Pepe' password: '1234'.
	carrito _ fachada createCarritoUser: 'Pepe' password: '1234'.
	fachada add: libro amount: 2 toCarrito: carrito.
	fachada checkout: carrito withCard: card.
	self assert: fachada totalCarritos = 0.! !

!FachadaTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 14:21:02'!
test07ShouldNotAllowListingPurchasesWithInvalidCredentials
	| carrito |
	fachada register: 'Pepe' password: '1234'.
	carrito _ fachada createCarritoUser: 'Pepe' password: '1234'.
	fachada add: libro amount: 2 toCarrito: carrito.
	fachada checkout: carrito withCard: card.
	self should: [fachada listPurchasesFrom: 'Pepito' password: '1234'.] 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :error | 
			self assert: error messageText = Fachada errorNoExisteTalUser.]! !

!FachadaTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 14:21:08'!
test08ShouldListAmountOfPurchasesCorrectly
	| carrito list |
	fachada register: 'Pepe' password: '1234'.
	carrito _ fachada createCarritoUser: 'Pepe' password: '1234'.
	fachada add: libro amount: 2 toCarrito: carrito.
	fachada checkout: carrito withCard: card.
	list _ fachada listPurchasesFrom: 'Pepe' password: '1234'.
	self assert: list size = 1.! !

!FachadaTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 14:21:14'!
test09DifferentUsersShouldHaveDifferentPurchases
	| carrito listOfPurchases otherListOfPurchases |
	fachada register: 'Pepe' password: '1234'.
	carrito _ fachada createCarritoUser: 'Pepe' password: '1234'.
	fachada add: libro amount: 2 toCarrito: carrito.
	fachada checkout: carrito withCard: card.
	fachada register: 'Jose' password: '1234'.
	carrito _ fachada createCarritoUser: 'Jose' password: '1234'.
	fachada add: libro amount: 3 toCarrito: carrito.
	fachada checkout: carrito withCard: card.
	listOfPurchases _ fachada listPurchasesFrom: 'Pepe' password: '1234'.
	otherListOfPurchases _ fachada listPurchasesFrom: 'Jose' password: '1234'.
	self deny: listOfPurchases = otherListOfPurchases. ! !

!FachadaTest methodsFor: 'tests' stamp: 'SCN 6/18/2018 14:29:27'!
test10ShouldNotAllowRegisteringSameUser
	fachada
		register: 'Pepe'
		password: '1234'.
	self
		should: [
			fachada
				register: 'Pepe'
				password: '1234' ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :error |
			self assert: error messageText = Fachada errorUserExistente ].! !

!FachadaTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 14:42:10'!
test11ShouldNotAllowCheckoutOfCarritoTwice
	| carrito |
	fachada register: 'Pepe' password: '1234'.
	carrito _ fachada createCarritoUser: 'Pepe' password: '1234'.
	fachada add: libro amount: 2 toCarrito: carrito.
	fachada checkout: carrito withCard: card.
	self should: [fachada checkout: carrito withCard: card] 
		raise: Error - MessageNotUnderstood
		withExceptionDo: [:error | self assert: error messageText = Fachada errorCarritoInvalido].! !

!FachadaTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 14:54:47'!
test12ShouldNotAllowListingInvalidCarrito
	self should: [fachada listCarrito: 3] 
		raise: Error - MessageNotUnderstood
		withExceptionDo: [:error | self assert: error messageText = Fachada errorCarritoInvalido].! !

!FachadaTest methodsFor: 'tests' stamp: 'JOP 6/18/2018 14:56:16'!
test13ShouldNotAllowAddingStuffToInvalidCarrito
	self should: [fachada add: libro amount: 2 toCarrito: 4] 
		raise: Error - MessageNotUnderstood
		withExceptionDo: [:error | self assert: error messageText = Fachada errorCarritoInvalido].! !


!FachadaTest methodsFor: 'testing setup' stamp: 'JOP 6/18/2018 13:22:25'!
setUp
	libro _ 'Fundación'.
	precios _ Dictionary new 
		at: 'Fundación' put: 50;	
		at: 'Fundación e Imperio' put: 100;
		at: 'Segunda Fundación' put: 200; yourself.
	fachada _ Fachada withPrecios: precios.
 	card _ CreditCard
		of: 'Grounded Time Traveler'
		withNumber: '2'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
		! !


!classDefinition: #Carrito category: #TusLibros!
Object subclass: #Carrito
	instanceVariableNames: 'libros catalogo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Carrito methodsFor: 'testing' stamp: 'JOP 6/6/2018 22:50:10'!
isEmpty
	^ libros isEmpty! !


!Carrito methodsFor: 'modifying' stamp: 'SCN 6/18/2018 15:59:32'!
add: aLibro
	(catalogo includes: aLibro) ifFalse: [ self error: self class errorNoExisteTalLibro ].
	libros add: aLibro.! !

!Carrito methodsFor: 'modifying' stamp: 'SCN 6/18/2018 15:59:32'!
add: aLibro amount: anAmount
	(catalogo includes: aLibro) ifFalse: [ self error: self class errorNoExisteTalLibro ].
	(anAmount > 0 and: [ anAmount isInteger ]) ifFalse: [ self error: self class errorNoSePuedenAgregarCopiasNoNaturales ].
	libros
		add: aLibro
		withOccurrences: anAmount.! !

!Carrito methodsFor: 'modifying' stamp: 'JOP 6/6/2018 23:39:33'!
remove: aLibro 
	libros remove: aLibro ifAbsent: [self error: self class errorLibroNoEnCarrito]! !


!Carrito methodsFor: 'initialization' stamp: 'SCN 6/18/2018 15:59:49'!
initializeWith: aCatalogo
	libros _ OrderedCollection new.
	catalogo _ aCatalogo.! !


!Carrito methodsFor: 'accessing' stamp: 'JOP 6/16/2018 14:01:59'!
includes: aLibro 
	^ libros includes: aLibro! !

!Carrito methodsFor: 'accessing' stamp: 'SCN 6/7/2018 02:11:08'!
list
	^libros copy.! !

!Carrito methodsFor: 'accessing' stamp: 'JOP 6/16/2018 14:09:55'!
occurrencesOf: aLibro 
	^ libros occurrencesOf: aLibro! !


!Carrito methodsFor: 'enumeration' stamp: 'SCN 6/16/2018 18:28:17'!
inject: anObject into: aBlockClosure 
	^libros inject: anObject into: aBlockClosure! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Carrito class' category: #TusLibros!
Carrito class
	instanceVariableNames: ''!

!Carrito class methodsFor: 'error' stamp: 'JOP 6/6/2018 23:30:56'!
errorLibroNoEnCarrito
	^ 'No existe tal libro en el carrito'! !

!Carrito class methodsFor: 'error' stamp: 'JOP 6/16/2018 14:16:16'!
errorNoExisteTalLibro
	^ 'No existe tal libro en el catálogo'! !

!Carrito class methodsFor: 'error' stamp: 'JOP 6/16/2018 14:21:44'!
errorNoSePuedenAgregarCopiasNoNaturales
	^ 'No se puede agregar una cantidad de copias no natural'! !


!Carrito class methodsFor: 'instance creation' stamp: 'JOP 6/18/2018 08:04:39'!
withCatalog: aCatalog
	^ self new initializeWith: aCatalog.! !


!classDefinition: #CarritoDeUser category: #TusLibros!
Object subclass: #CarritoDeUser
	instanceVariableNames: 'carrito user'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CarritoDeUser methodsFor: 'accessing' stamp: 'JOP 6/18/2018 13:59:44'!
carrito
	^carrito! !

!CarritoDeUser methodsFor: 'accessing' stamp: 'JOP 6/18/2018 13:59:48'!
user
	^user! !


!CarritoDeUser methodsFor: 'initialization' stamp: 'JOP 6/18/2018 13:58:26'!
initializeWithCarrito: aCarrito withUser: anUser
	carrito _ aCarrito.
	user _ anUser.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CarritoDeUser class' category: #TusLibros!
CarritoDeUser class
	instanceVariableNames: ''!

!CarritoDeUser class methodsFor: 'instance creation' stamp: 'JOP 6/18/2018 13:57:49'!
carrito: aCarrito user: anUser
	^self new initializeWithCarrito: aCarrito withUser: anUser.! !


!classDefinition: #Cashier category: #TusLibros!
Object subclass: #Cashier
	instanceVariableNames: 'listaDePrecios ventas'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'initialization' stamp: 'JOP 6/18/2018 09:55:25'!
initializeWith: aListaDePrecios
	listaDePrecios _ aListaDePrecios.
	ventas _ OrderedCollection new.! !


!Cashier methodsFor: 'processing' stamp: 'JOP 6/18/2018 11:52:33'!
checkout: aCarrito withCreditCard: aCreditCard withProcessor: aProcessor
	| venta total |
	aCarrito isEmpty ifTrue: [ self error: self class errorCheckoutDeCarritoVacio ].
	aCreditCard isExpired ifTrue: [ self error: self class errorTarjetaVencida ].
	total _ aCarrito inject: 0 into: [:acc :libro | acc + (listaDePrecios at: libro) ].
	aProcessor debit: total from: aCreditCard.
	venta _ Sale of: aCarrito list total: total.
	ventas add: venta.
	^total.! !


!Cashier methodsFor: 'accessing' stamp: 'JOP 6/18/2018 10:01:38'!
includes: aSale
	^ventas includes: aSale! !

!Cashier methodsFor: 'accessing' stamp: 'JOP 6/18/2018 10:08:37'!
isEmpty
	^ventas isEmpty.! !

!Cashier methodsFor: 'accessing' stamp: 'JOP 6/18/2018 10:18:45'!
list
	^ventas copy.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: #TusLibros!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'instance creation' stamp: 'JOP 6/18/2018 08:31:19'!
withPrecios: aListaDePrecios
	^self new initializeWith: aListaDePrecios.! !


!Cashier class methodsFor: 'error' stamp: 'SCN 6/16/2018 15:20:40'!
errorCheckoutDeCarritoVacio
	^ 'No se puede hacer checkout de un carrito vacío.'.! !

!Cashier class methodsFor: 'error' stamp: 'SCN 6/16/2018 16:17:18'!
errorTarjetaVencida
	^'La tarjeta de crédito está vencida.'! !


!classDefinition: #CreditCard category: #TusLibros!
Object subclass: #CreditCard
	instanceVariableNames: 'name number expiration'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'initialization' stamp: 'SCN 6/16/2018 16:12:49'!
initializeWithOwner: aName withNumber: aCreditCardNumber andExpiration: aCreditCardExpiration 
	name _ aName.
	number _ aCreditCardNumber.
	expiration _ aCreditCardExpiration.! !


!CreditCard methodsFor: 'accessing' stamp: 'SCN 6/16/2018 16:20:38'!
isExpired
	^expiration < GregorianMonthOfYear current! !

!CreditCard methodsFor: 'accessing' stamp: 'JOP 6/18/2018 11:54:55'!
name
	^name! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: #TusLibros!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'SCN 6/16/2018 16:19:37'!
of: aName withNumber: aCreditCardNumber andExpiration: aCreditCardExpiration
	^self new initializeWithOwner: aName withNumber: aCreditCardNumber andExpiration: aCreditCardExpiration.! !


!classDefinition: #Fachada category: #TusLibros!
Object subclass: #Fachada
	instanceVariableNames: 'carritos users catalogo listaDePrecios userCashiers ultimoIDCarrito'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Fachada methodsFor: 'creating' stamp: 'JOP 6/18/2018 14:48:33'!
createCarritoUser: anUser password: aPassword
	self checkUser: anUser password: aPassword.
	ultimoIDCarrito _ ultimoIDCarrito + 1.
	carritos at: ultimoIDCarrito put: (CarritoDeUser carrito: (Carrito withCatalog: catalogo) user: anUser).
	^ ultimoIDCarrito! !

!Fachada methodsFor: 'creating' stamp: 'JOP 6/18/2018 14:17:54'!
register: anUser password: aPassword
	(users includesKey: anUser) ifTrue: [self error: self class errorUserExistente].
	users at: anUser put: aPassword.
	userCashiers at: anUser put: (Cashier withPrecios: listaDePrecios).! !


!Fachada methodsFor: 'accessing' stamp: 'JOP 6/18/2018 14:55:23'!
listCarrito: aCarritoID
	^ (carritos at: aCarritoID ifAbsent: [self error: self class errorCarritoInvalido]) carrito list! !

!Fachada methodsFor: 'accessing' stamp: 'JOP 6/18/2018 14:13:01'!
listPurchasesFrom: anUser password: aPassword
	self checkUser: anUser password: aPassword.
	^ (userCashiers at: anUser) list.! !

!Fachada methodsFor: 'accessing' stamp: 'JOP 6/18/2018 13:07:14'!
totalCarritos
	^ carritos size.! !


!Fachada methodsFor: 'authentication - private' stamp: 'JOP 6/18/2018 14:00:24'!
checkUser: anUser password: aPassword
	(users includesKey: anUser) ifFalse: [self error: self class errorNoExisteTalUser].
	((users at: anUser) = aPassword) ifFalse: [self error: self class errorContraseñaInvalida].! !


!Fachada methodsFor: 'purchase' stamp: 'JOP 6/18/2018 14:56:51'!
add: aLibro amount: anAmount toCarrito: aCarritoID
	(carritos at: aCarritoID ifAbsent: [self error: self class errorCarritoInvalido]) carrito add: aLibro amount: anAmount.! !

!Fachada methodsFor: 'purchase' stamp: 'JOP 6/18/2018 14:52:40'!
checkout: aCarritoID withCard: aCreditCard
	| carritoDeUser cashier |
	carritoDeUser _ carritos at: aCarritoID ifAbsent: [self error: self class errorCarritoInvalido].
	cashier _ userCashiers at: carritoDeUser user.
	cashier checkout: carritoDeUser carrito withCreditCard: aCreditCard withProcessor: PaymentProcessor new.
	carritos removeKey: aCarritoID.! !


!Fachada methodsFor: 'initialization' stamp: 'JOP 6/18/2018 14:49:58'!
initializeWith: aListaDePrecios
	listaDePrecios _ aListaDePrecios.
	catalogo _ listaDePrecios keys.
	userCashiers _ Dictionary new.
	carritos _ Dictionary new.
	users _ Dictionary new.
	ultimoIDCarrito _ 0.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Fachada class' category: #TusLibros!
Fachada class
	instanceVariableNames: ''!

!Fachada class methodsFor: 'instance creation' stamp: 'JOP 6/18/2018 13:22:57'!
withPrecios: aListaDePrecios
	^ self new initializeWith: aListaDePrecios! !


!Fachada class methodsFor: 'error' stamp: 'JOP 6/18/2018 14:51:29'!
errorCarritoInvalido
	^ 'ID de carrito invalida'! !

!Fachada class methodsFor: 'error' stamp: 'JOP 6/18/2018 12:38:21'!
errorContraseñaInvalida
	^ 'No existe tal user'! !

!Fachada class methodsFor: 'error' stamp: 'JOP 6/18/2018 12:34:59'!
errorNoExisteTalUser
	^ 'No existe tal user'! !

!Fachada class methodsFor: 'error' stamp: 'JOP 6/18/2018 14:15:10'!
errorUserExistente
	^ 'Ya existe tal user'! !


!classDefinition: #PaymentProcessor category: #TusLibros!
Object subclass: #PaymentProcessor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!PaymentProcessor methodsFor: 'processing' stamp: 'JOP 6/18/2018 11:53:46'!
debit: amountOfMoney from: aCreditCard
	('Malos Fondos' = aCreditCard name) ifTrue: [self error: self class errorNotEnoughFunds ].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PaymentProcessor class' category: #TusLibros!
PaymentProcessor class
	instanceVariableNames: ''!

!PaymentProcessor class methodsFor: 'error' stamp: 'JOP 6/18/2018 11:50:46'!
errorNotEnoughFunds
	^'No hay suficientes fondos'! !


!classDefinition: #Sale category: #TusLibros!
Object subclass: #Sale
	instanceVariableNames: 'items total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Sale methodsFor: 'initialization' stamp: 'JOP 6/18/2018 09:15:12'!
initializeWithItems: listOfItems andTotal: aTotalPrice
	items _ listOfItems.
	total _ aTotalPrice.! !


!Sale methodsFor: 'accessing' stamp: 'JOP 6/18/2018 09:16:46'!
items
	^items! !

!Sale methodsFor: 'accessing' stamp: 'JOP 6/18/2018 09:16:53'!
total
	^total! !


!Sale methodsFor: 'comparing' stamp: 'JOP 6/18/2018 10:03:41'!
= aSale
	^ items = aSale items and: [total = aSale total].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sale class' category: #TusLibros!
Sale class
	instanceVariableNames: ''!

!Sale class methodsFor: 'instance creation' stamp: 'JOP 6/18/2018 09:14:08'!
of: aListOfItems total: aTotalPrice
	^Sale new initializeWithItems: aListOfItems andTotal: aTotalPrice.! !
