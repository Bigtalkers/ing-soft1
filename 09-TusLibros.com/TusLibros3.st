!classDefinition: #CarritoTest category: #TusLibros!
TestCase subclass: #CarritoTest
	instanceVariableNames: 'libro catalogo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 08:14:01'!
setUp
	libro _ '817525766-0'.
	catalogo _ Set new.
	catalogo add: libro.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 08:14:32'!
test01CarritoShouldStartEmpty
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	self assert: carrito isEmpty.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 08:14:36'!
test02CarritoShouldAllowAddLibro
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	carrito add: libro.
	self deny: carrito isEmpty.
	self assert: [ carrito includes: libro ].! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 08:14:39'!
test03CarritoShouldAllowRemoveLibro
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	carrito add: libro.
	carrito remove: libro.
	self assert: carrito isEmpty.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 08:14:41'!
test04CarritoShouldNotAllowRemoveNotIncluded
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	self
		should: [ carrito remove: libro ]
		raise: Error
		withExceptionDo: [ :error |
			self assert: error messageText = Carrito errorLibroNoEnCarrito ].! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 08:14:44'!
test05CarritoShouldListTheAddedLibros
	| carrito libros |
	carrito _ Carrito withCatalog: catalogo.
	carrito add: libro.
	libros _ OrderedCollection new.
	libros add: libro.
	self
		assert: libros
		equals: carrito list.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 08:14:46'!
test06CarritoShouldAllowAddingMultipleCopies
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	carrito
		add: libro
		amount: 2.
	self deny: carrito isEmpty.
	self
		assert: (carrito occurrencesOf: libro)
		equals: 2.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 08:52:00'!
test07CarritoShouldNotAllowAddingLibrosNotInCatalogo
	| carrito libro catalogo |
	libro _ '817525766-0'.
	catalogo _ OrderedCollection new.
	carrito _ Carrito withCatalog: catalogo.
	self
		should: [
			carrito
				add: libro
				amount: 2 ]
		raise: Error
		withExceptionDo: [ :error |
			self assert: error messageText = Carrito errorNoExisteTalLibro.
			self assert: carrito isEmpty ].! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 08:14:51'!
test08CarritoShouldNotAllowAddingNonNaturalCopies
	| carrito |
	carrito _ Carrito withCatalog: catalogo.
	self
		should: [
			carrito
				add: libro
				amount: 0 ]
		raise: Error
		withExceptionDo: [ :error |
			self assert: error messageText = Carrito errorNoSePuedenAgregarCopiasNoNaturales.
			self assert: carrito isEmpty ].
	self
		should: [
			carrito
				add: libro
				amount: 1 / 2 ]
		raise: Error
		withExceptionDo: [ :error |
			self assert: error messageText = Carrito errorNoSePuedenAgregarCopiasNoNaturales.
			self assert: carrito isEmpty ].! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 09:06:02'!
test09CarritoShouldInjectIntoContentsCorrectly
	| carrito libros |
	carrito _ Carrito withCatalog: catalogo.
	carrito add: libro.
	libros _ OrderedCollection new.
	carrito inject: libros into: [:acum :elem | acum add: elem].
	self assert: libros size = 1.
	self assert: (libros includes: libro).! !


!classDefinition: #CashierTest category: #TusLibros!
TestCase subclass: #CashierTest
	instanceVariableNames: 'precios catalogo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 08:30:28'!
setUp
	precios _ Dictionary new 
		at: 'Fundaci�n' put: 50;	
		at: 'Fundaci�n e Imperio' put: 100;
		at: 'Segunda Fundaci�n' put: 200; yourself.
	catalogo _ precios keys.! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 11:17:36'!
test01ShouldNotAllowEmptyCheckout
	| cashier carrito card |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	card _ CreditCard
		of: 'Grounded Time Traveler'
		withNumber: '2'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
	self
		should: [
			cashier
				checkout: carrito
				withCreditCard: card 
				withProcessor: PaymentProcessor new]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :error |
			self assert: error messageText = Cashier errorCheckoutDeCarritoVacio.
			self assert: cashier isEmpty].! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 11:17:10'!
test02ShouldNotAllowExpiredCard
	| cashier carrito card |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	carrito add: 'Fundaci�n'.
	card _ CreditCard
		of: 'Clueless Time Traveler'
		withNumber: '1'
		andExpiration: (GregorianMonthOfYear januaryOf: GregorianYear current).
	self
		should: [
			cashier
				checkout: carrito
				withCreditCard: card 
				withProcessor: PaymentProcessor new]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :error |
			self assert: error messageText = Cashier errorTarjetaVencida.
			self assert: cashier isEmpty].! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 11:16:35'!
test03ShouldCalculateTotalCorrectly
	| cashier carrito card total |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	carrito
		add: 'Fundaci�n'
		amount: 2;
		add: 'Fundaci�n e Imperio'; 
		yourself.
	card _ CreditCard
		of: 'Grounded Time Traveler'
		withNumber: '2'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
	total _ cashier checkout: carrito withCreditCard: card withProcessor: PaymentProcessor new.
	self assert: 200 = total.! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 11:17:49'!
test04ShouldIncludeSalesCorrectly
	| cashier carrito card |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	carrito
		add: 'Fundaci�n'
		amount: 2;
		add: 'Fundaci�n e Imperio'; 
		yourself.
	card _ CreditCard
		of: 'Grounded Time Traveler'
		withNumber: '2'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
	cashier checkout: carrito withCreditCard: card withProcessor: PaymentProcessor new.
	self assert: (cashier includes: (Sale of: carrito list total: 200)).! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 11:17:59'!
test05ShouldListSalesCorrectly
	| cashier carrito card |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	carrito
		add: 'Fundaci�n'
		amount: 2;
		add: 'Fundaci�n e Imperio'; 
		yourself.
	card _ CreditCard
		of: 'Grounded Time Traveler'
		withNumber: '2'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
	cashier checkout: carrito withCreditCard: card withProcessor: PaymentProcessor new.
	self assert: (cashier list hasEqualElements: (Array with: (Sale of: carrito list total: 200))).! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 11:18:08'!
test06ShouldAllowProcessorForCheckout
	| cashier carrito card processor |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	processor _ PaymentProcessor new.
	carrito
		add: 'Fundaci�n'
		amount: 2;
		add: 'Fundaci�n e Imperio'; 
		yourself.
	card _ CreditCard
		of: 'Grounded Time Traveler'
		withNumber: '2'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
	cashier checkout: carrito withCreditCard: card withProcessor: processor.
	self assert: (cashier list hasEqualElements: (Array with: (Sale of: carrito list total: 200))).! !

!CashierTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 11:22:14'!
test07ShouldNotAllowCheckoutWithoutEnoughFunds
	| cashier carrito card processor |
	carrito _ Carrito withCatalog: catalogo.
	cashier _ Cashier withPrecios: precios.
	processor _ PaymentProcessor new.
	carrito
		add: 'Fundaci�n'
		amount: 2;
		add: 'Fundaci�n e Imperio'; 
		yourself.
	card _ CreditCard
		of: 'Malos Fondos'
		withNumber: '3'
		andExpiration: (GregorianMonthOfYear januaryOf: (GregorianYear number: 2019)).
	self should: [cashier checkout: carrito withCreditCard: card withProcessor: processor.] 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :error | 
			self assert: error messageText = PaymentProcessor errorNotEnoughFunds.
			self assert: cashier isEmpty.
			]! !


!classDefinition: #Carrito category: #TusLibros!
Object subclass: #Carrito
	instanceVariableNames: 'libros tienda'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Carrito methodsFor: 'testing' stamp: 'JOP 6/6/2018 22:50:10'!
isEmpty
	^ libros isEmpty! !


!Carrito methodsFor: 'modifying' stamp: 'JOP 6/18/2018 07:01:50'!
add: aLibro
	(tienda includes: aLibro) ifFalse: [ self error: Catalogo errorNoExisteTalLibro ].
	libros add: aLibro.! !

!Carrito methodsFor: 'modifying' stamp: 'SCN 6/16/2018 18:05:24'!
add: aLibro amount: anAmount
	(tienda includes: aLibro) ifFalse: [ self error: Carrito errorNoExisteTalLibro ].
	(anAmount > 0 and: anAmount isInteger) ifFalse: [ self error: Carrito errorNoSePuedenAgregarCopiasNoNaturales ].
	libros
		add: aLibro
		withOccurrences: anAmount.! !

!Carrito methodsFor: 'modifying' stamp: 'JOP 6/6/2018 23:39:33'!
remove: aLibro 
	libros remove: aLibro ifAbsent: [self error: self class errorLibroNoEnCarrito]! !


!Carrito methodsFor: 'initialization' stamp: 'SCN 6/16/2018 18:05:46'!
initializeWith: aTienda
	libros _ OrderedCollection new.
	tienda _ aTienda.! !


!Carrito methodsFor: 'accessing' stamp: 'JOP 6/16/2018 14:01:59'!
includes: aLibro 
	^ libros includes: aLibro! !

!Carrito methodsFor: 'accessing' stamp: 'SCN 6/7/2018 02:11:08'!
list
	^libros copy.! !

!Carrito methodsFor: 'accessing' stamp: 'JOP 6/16/2018 14:09:55'!
occurrencesOf: aLibro 
	^ libros occurrencesOf: aLibro! !


!Carrito methodsFor: 'enumeration' stamp: 'SCN 6/16/2018 18:28:17'!
inject: anObject into: aBlockClosure 
	^libros inject: anObject into: aBlockClosure! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Carrito class' category: #TusLibros!
Carrito class
	instanceVariableNames: ''!

!Carrito class methodsFor: 'errors' stamp: 'JOP 6/6/2018 23:30:56'!
errorLibroNoEnCarrito
	^ 'No existe tal libro en el carrito'! !

!Carrito class methodsFor: 'errors' stamp: 'JOP 6/16/2018 14:16:16'!
errorNoExisteTalLibro
	^ 'No existe tal libro en el cat�logo'! !

!Carrito class methodsFor: 'errors' stamp: 'JOP 6/16/2018 14:21:44'!
errorNoSePuedenAgregarCopiasNoNaturales
	^ 'No se puede agregar una cantidad de copias no natural'! !


!Carrito class methodsFor: 'instance creation' stamp: 'JOP 6/18/2018 08:04:39'!
withCatalog: aCatalog
	^ self new initializeWith: aCatalog.! !


!classDefinition: #Cashier category: #TusLibros!
Object subclass: #Cashier
	instanceVariableNames: 'listaDePrecios ventas'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'initialization' stamp: 'JOP 6/18/2018 10:01:38'!
includes: aSale
	^ventas includes: aSale! !

!Cashier methodsFor: 'initialization' stamp: 'JOP 6/18/2018 09:55:25'!
initializeWith: aListaDePrecios
	listaDePrecios _ aListaDePrecios.
	ventas _ OrderedCollection new.! !

!Cashier methodsFor: 'initialization' stamp: 'JOP 6/18/2018 10:08:37'!
isEmpty
	^ventas isEmpty.! !


!Cashier methodsFor: 'processing' stamp: 'JOP 6/18/2018 11:52:33'!
checkout: aCarrito withCreditCard: aCreditCard withProcessor: aProcessor
	| venta total |
	aCarrito isEmpty ifTrue: [ self error: self class errorCheckoutDeCarritoVacio ].
	aCreditCard isExpired ifTrue: [ self error: self class errorTarjetaVencida ].
	total _ aCarrito inject: 0 into: [:acc :libro | acc + (listaDePrecios at: libro) ].
	aProcessor debit: total from: aCreditCard.
	venta _ Sale of: aCarrito list total: total.
	ventas add: venta.
	^total.! !


!Cashier methodsFor: 'accessing' stamp: 'JOP 6/18/2018 10:18:45'!
list
	^ventas copy.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: #TusLibros!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'instance creation' stamp: 'JOP 6/18/2018 08:31:19'!
withPrecios: aListaDePrecios
	^self new initializeWith: aListaDePrecios.! !


!Cashier class methodsFor: 'error messages' stamp: 'SCN 6/16/2018 15:20:40'!
errorCheckoutDeCarritoVacio
	^ 'No se puede hacer checkout de un carrito vac�o.'.! !

!Cashier class methodsFor: 'error messages' stamp: 'SCN 6/16/2018 16:17:18'!
errorTarjetaVencida
	^'La tarjeta de cr�dito est� vencida.'! !


!classDefinition: #CreditCard category: #TusLibros!
Object subclass: #CreditCard
	instanceVariableNames: 'name number expiration'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'initialization' stamp: 'SCN 6/16/2018 16:12:49'!
initializeWithOwner: aName withNumber: aCreditCardNumber andExpiration: aCreditCardExpiration 
	name _ aName.
	number _ aCreditCardNumber.
	expiration _ aCreditCardExpiration.! !


!CreditCard methodsFor: 'accessing' stamp: 'SCN 6/16/2018 16:20:38'!
isExpired
	^expiration < GregorianMonthOfYear current! !

!CreditCard methodsFor: 'accessing' stamp: 'JOP 6/18/2018 11:54:55'!
name
	^name! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: #TusLibros!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'SCN 6/16/2018 16:19:37'!
of: aName withNumber: aCreditCardNumber andExpiration: aCreditCardExpiration
	^self new initializeWithOwner: aName withNumber: aCreditCardNumber andExpiration: aCreditCardExpiration.! !


!classDefinition: #PaymentProcessor category: #TusLibros!
Object subclass: #PaymentProcessor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!PaymentProcessor methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 11:53:46'!
debit: amountOfMoney from: aCreditCard
	('Malos Fondos' = aCreditCard name) ifTrue: [self error: self class errorNotEnoughFunds ].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PaymentProcessor class' category: #TusLibros!
PaymentProcessor class
	instanceVariableNames: ''!

!PaymentProcessor class methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 11:50:46'!
errorNotEnoughFunds
	^'No hay suficientes fondos'! !


!classDefinition: #Sale category: #TusLibros!
Object subclass: #Sale
	instanceVariableNames: 'items total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Sale methodsFor: 'initialization' stamp: 'JOP 6/18/2018 09:15:12'!
initializeWithItems: listOfItems andTotal: aTotalPrice
	items _ listOfItems.
	total _ aTotalPrice.! !


!Sale methodsFor: 'accessing' stamp: 'JOP 6/18/2018 09:16:46'!
items
	^items! !

!Sale methodsFor: 'accessing' stamp: 'JOP 6/18/2018 09:16:53'!
total
	^total! !


!Sale methodsFor: 'comparing' stamp: 'JOP 6/18/2018 10:03:41'!
= aSale
	^ items = aSale items and: [total = aSale total].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sale class' category: #TusLibros!
Sale class
	instanceVariableNames: ''!

!Sale class methodsFor: 'as yet unclassified' stamp: 'JOP 6/18/2018 09:14:08'!
of: aListOfItems total: aTotalPrice
	^Sale new initializeWithItems: aListOfItems andTotal: aTotalPrice.! !
