!classDefinition: #TicTacToeTest category: #TicTacToe!
TestCase subclass: #TicTacToeTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'SCN 5/31/2018 14:56:29'!
test01GameStartsEmpty

	|game|
	
	game := TicTacToe new.
	
	self assert: game Xs isEmpty.
	self assert: game Os isEmpty.
	self assert: game XIsPlaying.
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'SCN 5/31/2018 14:56:47'!
test02XCanStartCorrectly
	| game |
	game _ TicTacToe new.
	game putXAt: 1 @ 1.
	self
		assert: 1
		equals: game Xs size.
	self assert: (game Xs includes: 1 @ 1).
	self assert: game Os isEmpty.
	self assert: game OIsPlaying.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:40:32'!
test03OCanFollowCorrectly

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	
	self assert: 1 equals: game Xs size.
	self assert: (game Xs includes: 1@1).
	self assert: 1 equals: game Os size.
	self assert: (game Os includes: 2@2).
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:40:54'!
test04XCannotPlayTwiceInARow

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	
	self
	should: [game putXAt: 2@2]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe notXTurnErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: game Os isEmpty.
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:41:20'!
test05OCannotPlayTwiceInARow

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	self
	should: [game putOAt: 3@3]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe notOTurnErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: 1 equals: game Os size.
		self assert: (game Os includes: 2@2).
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:42:32'!
test06XCannotPlayOnAnotherX
	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	self
	should: [game putXAt: 1@1]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe positionTakenErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: 1 equals: game Os size.
		self assert: (game Os includes: 2@2).
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:42:41'!
test07XCannotPlayOnAnotherO

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	self
	should: [game putXAt: 2@2]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe positionTakenErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: 1 equals: game Os size.
		self assert: (game Os includes: 2@2).
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:42:53'!
test08OCannotPlayOnAnotherX

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	self
	should: [game putOAt: 1@1]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe positionTakenErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert:  game Os isEmpty.

		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:43:05'!
test09OCannotPlayOnAnotherO

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	game putXAt: 3@3.
	self
	should: [game putOAt: 2@2]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe positionTakenErrorMessage equals: anError messageText.	
		self assert: 2 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: (game Xs includes: 3@3).
		self assert: 1 equals: game Os size.
		self assert: (game Os includes: 2@2).
		
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:43:38'!
test10PlayersShouldNotWinIfConditionsAreNotMet

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@1.
	game putXAt: 2@2.
	game putOAt: 3@3.
	
		self deny: game OHasWon.
		self deny: game XHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:44:57'!
test11XShouldWinIfCompletedFirstRow

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@1.
	game putXAt: 1@2.
	game putOAt: 3@3.
	game putXAt: 1@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:45:10'!
test12XShouldWinIfCompletedSecondRow

	|game|
	
	game := TicTacToe new.

	game putXAt: 2@1.
	game putOAt: 1@1.
	game putXAt: 2@2.
	game putOAt: 3@1.
	game putXAt: 2@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:45:21'!
test13XShouldWinIfCompletedThirdRow

	|game|
	
	game := TicTacToe new.

	game putXAt: 3@1.
	game putOAt: 1@1.
	game putXAt: 3@2.
	game putOAt: 2@1.
	game putXAt: 3@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:45:37'!
test14XShouldWinIfCompletedAColumn

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	game putXAt: 2@1.
	game putOAt: 3@3.
	game putXAt: 3@1.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:45:50'!
test15XShouldWinIfCompletedDownDiagonal

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@1.
	game putXAt: 2@2.
	game putOAt: 2@3.
	game putXAt: 3@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:46:07'!
test16XShouldWinIfCompletedUpDiagonal

	|game|
	
	game := TicTacToe new.

	game putXAt: 3@1.
	game putOAt: 2@1.
	game putXAt: 2@2.
	game putOAt: 1@2.
	game putXAt: 1@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:46:26'!
test17OShouldWinIfCompletedUpDiagonal

	|game|
	
	game := TicTacToe new.

	game putXAt: 3@1.
	game putOAt: 1@1.
	game putXAt: 2@2.
	game putOAt: 1@2.
	game putXAt: 3@3.
	game putOAt: 1@3.
	
		self assert: game OHasWon.
		self deny: game XHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:47:37'!
test18OShouldNotBeAbleToPlayIfGameOver

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 1@1.	
	game putOAt: 2@1.
	game putXAt: 1@2.
	game putOAt: 3@1.	
	game putXAt: 1@3.

	self 
		should: [ game putOAt: 3@1 ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: TicTacToe canNotPlayWhenGameIsOverErrorMessage equals: anError messageText.
			self assert: 2 equals: game Os size.
			self assert: (game Os includes: 2@1).
			self assert: (game Os includes: 3@1) ]
			
			! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:47:43'!
test19XShouldNotBeAbleToPlayIfGameOver

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 2@2.
	game putOAt: 1@1.	
	game putXAt: 2@1.
	game putOAt: 1@2.
	game putXAt: 3@1.	
	game putOAt: 1@3.

	self 
		should: [ game putXAt: 3@2 ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: TicTacToe canNotPlayWhenGameIsOverErrorMessage equals: anError messageText.
			self assert: 3 equals: game Xs size.
			self assert: (game Xs includes: 2@2).
			self assert: (game Xs includes: 2@1).
			self assert: (game Xs includes: 3@1) ]
			
			! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:48:35'!
test20GameShouldBeTiedIfNoMovesArePossibleAndNobodyWon

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 2@2.
	game putOAt: 1@1.	
	game putXAt: 1@3.
	game putOAt: 3@1.
	game putXAt: 2@1.	
	game putOAt: 2@3.
	game putXAt: 1@2.	
	game putOAt: 3@2.
	game putXAt: 3@3.
	
	self assert: game isOver.
	self deny: game XHasWon. 
	self deny: game OHasWon. 
	self assert: game isTied
! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:49:08'!
test21XShouldWinEvenIfNoMovesArePossibleIfConditionsAreMet

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 2@2.
	game putOAt: 1@2.	
	game putXAt: 1@1.
	game putOAt: 2@1.
	game putXAt: 1@3.	
	game putOAt: 2@3.
	game putXAt: 3@2.	
	game putOAt: 3@1.
	game putXAt: 3@3.
	
	self assert: game isOver.
	self assert: game XHasWon. 
	self deny: game OHasWon. 
	self deny: game isTied
! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'SCN 5/31/2018 14:57:24'!
test22NobodyShouldBePlayingAtGameOver
	| game |
	game _ TicTacToe new.
	game putXAt: 1 @ 1.
	game putOAt: 2 @ 1.
	game putXAt: 1 @ 2.
	game putOAt: 3 @ 3.
	game putXAt: 1 @ 3.
	self deny: game XIsPlaying.
	self deny: game OIsPlaying.! !


!classDefinition: #TicTacToe category: #TicTacToe!
Object subclass: #TicTacToe
	instanceVariableNames: 'os xs state'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToe methodsFor: 'checking' stamp: 'JOP 5/31/2018 12:12:30'!
hasCompletedColumn: positions
	
	^ (1 to: 3) anySatisfy: [ :y | (positions count: [ :position | position y = y ]) = 3 ]! !

!TicTacToe methodsFor: 'checking' stamp: 'JOP 5/31/2018 12:12:41'!
hasCompletedRow: positions

	^ (1 to: 3) anySatisfy: [ :x | (positions count: [ :posicion | posicion x = x ]) = 3 ]! !

!TicTacToe methodsFor: 'checking' stamp: 'JOP 5/31/2018 12:13:11'!
hasDownDiagonal: positions

	^(1 to: 3) allSatisfy: [ :n | positions includes: n@n ]
! !

!TicTacToe methodsFor: 'checking' stamp: 'JOP 5/31/2018 12:13:21'!
hasUpDiagonal: positions

	^(1 to: 3) allSatisfy: [ :n | positions includes: n@(4-n) ]! !

!TicTacToe methodsFor: 'checking' stamp: 'JOP 5/31/2018 12:13:28'!
hasWin: positions

	^(self hasCompletedRow: positions)
		or: [(self hasCompletedColumn: positions) 
				or: [(self hasDownDiagonal: positions)
					or: [(self hasUpDiagonal: positions)]]]

! !


!TicTacToe methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:51:24'!
checkPositionIsFree: aPosition
	((xs includes: aPosition) or: (os includes: aPosition )) ifTrue: [ self error: self class positionTakenErrorMessage ].! !

!TicTacToe methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:06:01'!
putOAt: aPosition
	state := state putOWith: [self checkPositionIsFree: aPosition. os add: aPosition] checkWinWith: [self hasWin: os]! !

!TicTacToe methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:07:05'!
putXAt: aPosition
	state := state putXWith: [self checkPositionIsFree: aPosition. xs add: aPosition] checkWinWith: [self hasWin: xs]! !


!TicTacToe methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:49:24'!
OHasWon
	^state OHasWon! !

!TicTacToe methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:49:31'!
OIsPlaying
	^ state OIsPlaying! !

!TicTacToe methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:49:40'!
XHasWon
	^ state XHasWon! !

!TicTacToe methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:49:36'!
XIsPlaying
	^ state XIsPlaying! !

!TicTacToe methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:49:53'!
isOver
	^ state isOver! !

!TicTacToe methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:49:59'!
isTied
	^state isTied! !


!TicTacToe methodsFor: 'accessing' stamp: 'jg 5/22/2018 10:01:34'!
Os

	^os copy.! !

!TicTacToe methodsFor: 'accessing' stamp: 'jg 5/22/2018 10:01:25'!
Xs

	^xs copy.! !


!TicTacToe methodsFor: 'initialization' stamp: 'JOP 5/31/2018 12:22:21'!
initialize
	xs _ Set new.
	os _ Set new.
	state _ TicTacToeStateTurnX atTurn: 1.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TicTacToe class' category: #TicTacToe!
TicTacToe class
	instanceVariableNames: ''!

!TicTacToe class methodsFor: 'errors' stamp: 'JOP 5/31/2018 05:24:16'!
canNotPlayWhenGameIsOverErrorMessage
	
	^'Can not play when game is over'! !

!TicTacToe class methodsFor: 'errors' stamp: 'JOP 5/31/2018 05:23:55'!
notOTurnErrorMessage

	^'Not O turn'! !

!TicTacToe class methodsFor: 'errors' stamp: 'JOP 5/31/2018 05:22:57'!
notXTurnErrorMessage

	^'Not X turn'! !

!TicTacToe class methodsFor: 'errors' stamp: 'JOP 5/31/2018 05:24:28'!
positionTakenErrorMessage
	
	^'Position taken'! !


!classDefinition: #TicTacToeState category: #TicTacToe!
Object subclass: #TicTacToeState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeState methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:23:32'!
putOWith: aClosureForPut checkWinWith: aClosureForCheck
	self subclassResponsibility 
! !

!TicTacToeState methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:23:34'!
putXWith: aClosureForPut checkWinWith: aClosureForCheck
	self subclassResponsibility 
! !


!TicTacToeState methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:51:29'!
OHasWon
	self subclassResponsibility 

! !

!TicTacToeState methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:52:06'!
OIsPlaying
	self subclassResponsibility 

! !

!TicTacToeState methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:51:34'!
XHasWon
	self subclassResponsibility 

! !

!TicTacToeState methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:52:04'!
XIsPlaying
	self subclassResponsibility 

! !

!TicTacToeState methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:50:41'!
isOver
	self subclassResponsibility 

! !

!TicTacToeState methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:51:51'!
isTied
	self subclassResponsibility 

! !


!classDefinition: #TicTacToeStateGameOver category: #TicTacToe!
TicTacToeState subclass: #TicTacToeStateGameOver
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateGameOver methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:23:14'!
putOWith: aClosureForPut checkWinWith: aClosureForCheck
	self error: TicTacToe canNotPlayWhenGameIsOverErrorMessage.! !

!TicTacToeStateGameOver methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:23:19'!
putXWith: aClosureForPut checkWinWith: aClosureForCheck
	self error: TicTacToe canNotPlayWhenGameIsOverErrorMessage.! !


!TicTacToeStateGameOver methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:53:58'!
OHasWon
	^ self subclassResponsibility ! !

!TicTacToeStateGameOver methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:53:46'!
OIsPlaying
	^false! !

!TicTacToeStateGameOver methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:54:00'!
XHasWon
	^ self subclassResponsibility ! !

!TicTacToeStateGameOver methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:53:43'!
XIsPlaying
	^false! !

!TicTacToeStateGameOver methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:47:02'!
isOver
	^true! !

!TicTacToeStateGameOver methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:54:05'!
isTied
	^ self subclassResponsibility ! !


!classDefinition: #TicTacToeStateOWon category: #TicTacToe!
TicTacToeStateGameOver subclass: #TicTacToeStateOWon
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateOWon methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:04'!
OHasWon
	^ true! !

!TicTacToeStateOWon methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:10'!
XHasWon
	^ false! !

!TicTacToeStateOWon methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:14'!
isTied
	^ false! !


!classDefinition: #TicTacToeStateTied category: #TicTacToe!
TicTacToeStateGameOver subclass: #TicTacToeStateTied
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateTied methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:39'!
OHasWon
	^ false! !

!TicTacToeStateTied methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:37'!
XHasWon
	^ false! !

!TicTacToeStateTied methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:30'!
isTied
	^ true! !


!classDefinition: #TicTacToeStateXWon category: #TicTacToe!
TicTacToeStateGameOver subclass: #TicTacToeStateXWon
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateXWon methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:46'!
OHasWon
	^ false! !

!TicTacToeStateXWon methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:51'!
XHasWon
	^ true! !

!TicTacToeStateXWon methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:56'!
isTied
	^ false! !


!classDefinition: #TicTacToeStateTurn category: #TicTacToe!
TicTacToeState subclass: #TicTacToeStateTurn
	instanceVariableNames: 'turn'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateTurn methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:08:38'!
putOWith: aClosureForPut checkWinWith: aClosureForCheck
	^ self subclassResponsibility.! !

!TicTacToeStateTurn methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:08:44'!
putXWith: aClosureForPut checkWinWith: aClosureForCheck
	^ self subclassResponsibility.! !


!TicTacToeStateTurn methodsFor: 'initialization' stamp: 'JOP 5/31/2018 12:18:08'!
initializeWith: aTurnNumber
	turn := aTurnNumber ! !


!TicTacToeStateTurn methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:56:45'!
OHasWon
	^ false! !

!TicTacToeStateTurn methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:57:19'!
OIsPlaying
	^ self subclassResponsibility ! !

!TicTacToeStateTurn methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:56:42'!
XHasWon
	^ false! !

!TicTacToeStateTurn methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:57:18'!
XIsPlaying
	^ self subclassResponsibility ! !

!TicTacToeStateTurn methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:56:24'!
isOver
	^ false! !

!TicTacToeStateTurn methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:56:35'!
isTied
	^ false! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TicTacToeStateTurn class' category: #TicTacToe!
TicTacToeStateTurn class
	instanceVariableNames: ''!

!TicTacToeStateTurn class methodsFor: 'instance creation' stamp: 'JOP 5/31/2018 12:21:33'!
atTurn: aTurnNumber
	^self new initializeWith: aTurnNumber.! !


!classDefinition: #TicTacToeStateTurnO category: #TicTacToe!
TicTacToeStateTurn subclass: #TicTacToeStateTurnO
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateTurnO methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:22:01'!
putOWith: aClosureForPut checkWinWith: aClosureForCheck
	aClosureForPut value.
	aClosureForCheck value ifTrue: [ ^ TicTacToeStateOWon new ].
	^ TicTacToeStateTurnX atTurn: turn + 1.! !

!TicTacToeStateTurnO methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:10:15'!
putXWith: aClosureForPut checkWinWith: aClosureForCheck
	self error: TicTacToe notXTurnErrorMessage.! !


!TicTacToeStateTurnO methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:57:59'!
OIsPlaying
	^ true! !

!TicTacToeStateTurnO methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:57:39'!
XIsPlaying
	^ false! !


!classDefinition: #TicTacToeStateTurnX category: #TicTacToe!
TicTacToeStateTurn subclass: #TicTacToeStateTurnX
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateTurnX methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:15:18'!
putOWith: aClosureForPut checkWinWith: aClosureForCheck
	self error: TicTacToe notOTurnErrorMessage.! !

!TicTacToeStateTurnX methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:29:10'!
putXWith: aClosureForPut checkWinWith: aClosureForCheck
	aClosureForPut value.
	aClosureForCheck value ifTrue: [ ^ TicTacToeStateXWon new ].
	turn = 5 ifTrue: [ ^ TicTacToeStateTied new ].
	^ TicTacToeStateTurnO atTurn: turn.! !


!TicTacToeStateTurnX methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:57:53'!
OIsPlaying
	^ false! !

!TicTacToeStateTurnX methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:57:48'!
XIsPlaying
	^ true! !
