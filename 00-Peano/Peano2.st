!classDefinition: #I category: #Peano!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Peano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: #Peano!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'other' stamp: 'JP 3/29/2018 03:40:46'!
subtractTo: aNumber
	^ aNumber previous.! !


!I class methodsFor: 'operations' stamp: 'NOIT 3/26/2018 19:16:02'!
* unNumero
	^ unNumero! !

!I class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:10:01'!
+ unNumero
	^ unNumero next! !

!I class methodsFor: 'operations' stamp: 'JP 3/29/2018 01:48:54'!
- Number
	self error: self descripcionDeErrorDeNumerosNegativosNoSoportados! !

!I class methodsFor: 'operations' stamp: 'JP 3/29/2018 02:57:36'!
/ unNumero
	unNumero ifOne: [ ^ I ].
	self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor.! !

!I class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:10:16'!
next
	^ II! !

!I class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:41:39'!
previous
	self error: self descripcionDeErrorDeNumerosNegativosNoSoportados! !


!I class methodsFor: 'conditionals' stamp: 'JP 3/29/2018 02:59:18'!
ifGreaterThan: unNumero eval: unaClausura! !

!I class methodsFor: 'conditionals' stamp: 'JP 3/29/2018 02:57:36'!
ifOne: unaClausura
	unaClausura value.! !


!I class methodsFor: 'errors' stamp: 'JP 3/29/2018 01:21:51'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
	^'No se puede dividir por un numero mayor'! !

!I class methodsFor: 'errors' stamp: 'JP 3/28/2018 21:34:19'!
descripcionDeErrorDeNumerosNegativosNoSoportados
	^'Numeros negativos no son soportados'! !


!classDefinition: #II category: #Peano!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Peano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: #Peano!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:09:28'!
* unNumero
	^ previous * unNumero + unNumero! !

!II class methodsFor: 'operations' stamp: 'NOIT 3/22/2018 21:09:28'!
+ unNumero
	^ self previous + unNumero next! !

!II class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:07'!
- aNumber
	^ aNumber subtractTo: self! !

!II class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:12:48'!
/ unNumero
	unNumero	ifGreaterThan: self eval: [ self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor ].
	self ifGreaterThan: unNumero eval: [ ^ self - unNumero / unNumero + I ].
	^ I.! !

!II class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:09:39'!
next
	next ifNil: [
		next := self cloneNamed: (self name, 'I').
		next previous: self.
	].
	^ next! !

!II class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:09:43'!
previous
	^ previous! !


!II class methodsFor: 'conditionals' stamp: 'JP 3/29/2018 02:59:48'!
ifGreaterThan: unNumero eval: unaClausura
	unNumero ifOne: unaClausura.
	previous ifGreaterThan: unNumero previous eval: unaClausura.! !

!II class methodsFor: 'conditionals' stamp: 'JP 3/29/2018 02:59:37'!
ifOne: unaClausura! !


!II class methodsFor: 'errors' stamp: 'JP 3/29/2018 02:55:32'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
	^'No se puede dividir por un numero mayor'! !


!II class methodsFor: 'other' stamp: 'NOIT 3/22/2018 21:32:44'!
previous: aNumber
	previous := aNumber! !

!II class methodsFor: 'other' stamp: 'JP 3/29/2018 03:00:21'!
removeAllNext
	next ifNotNil: [
		next removeAllNext.
		next removeFromSystem.
		next := nil.
	]! !

!II class methodsFor: 'other' stamp: 'JP 3/29/2018 03:48:49'!
subtractTo: aNumber
	^ previous subtractTo: aNumber previous.! !


!II class methodsFor: 'class initialization' stamp: 'scn 4/1/2018 23:31:57'!
initialize
	"Inicialización de colaboradores internos next y previous."

	next := III.
	previous := I.! !

II instVarNamed: 'next' put: III!
II instVarNamed: 'previous' put: I!

!classDefinition: #III category: #Peano!
DenotativeObject subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Peano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: #Peano!
III class
	instanceVariableNames: 'next previous'!

!III class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:28'!
* unNumero
	^ previous * unNumero + unNumero! !

!III class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:28'!
+ unNumero
	^ self previous + unNumero next! !

!III class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:28'!
- aNumber
	^ aNumber subtractTo: self! !

!III class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:28'!
/ unNumero
	unNumero	ifGreaterThan: self eval: [ self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor ].
	self ifGreaterThan: unNumero eval: [ ^ self - unNumero / unNumero + I ].
	^ I.! !

!III class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:28'!
next
	next ifNil: [
		next := self cloneNamed: (self name, 'I').
		next previous: self.
	].
	^ next! !

!III class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:28'!
previous
	^ previous! !


!III class methodsFor: 'conditionals' stamp: 'JP 3/29/2018 03:50:28'!
ifGreaterThan: unNumero eval: unaClausura
	unNumero ifOne: unaClausura.
	previous ifGreaterThan: unNumero previous eval: unaClausura.! !

!III class methodsFor: 'conditionals' stamp: 'JP 3/29/2018 03:50:28'!
ifOne: unaClausura! !


!III class methodsFor: 'errors' stamp: 'JP 3/29/2018 03:50:28'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
	^'No se puede dividir por un numero mayor'! !


!III class methodsFor: 'class initialization' stamp: 'scn 4/1/2018 23:35:47'!
initialize
	"Inicialización de colaboradores internos next y previous."

	next := IIII.
	previous := II.! !


!III class methodsFor: 'other' stamp: 'JP 3/29/2018 03:50:28'!
previous: aNumber
	previous := aNumber! !

!III class methodsFor: 'other' stamp: 'JP 3/29/2018 03:50:28'!
removeAllNext
	next ifNotNil: [
		next removeAllNext.
		next removeFromSystem.
		next := nil.
	]! !

!III class methodsFor: 'other' stamp: 'JP 3/29/2018 03:50:28'!
subtractTo: aNumber
	^ previous subtractTo: aNumber previous.! !

III instVarNamed: 'next' put: IIII!
III instVarNamed: 'previous' put: II!

!classDefinition: #IIII category: #Peano!
DenotativeObject subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Peano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: #Peano!
IIII class
	instanceVariableNames: 'next previous'!

!IIII class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:28'!
* unNumero
	^ previous * unNumero + unNumero! !

!IIII class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:28'!
+ unNumero
	^ self previous + unNumero next! !

!IIII class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:28'!
- aNumber
	^ aNumber subtractTo: self! !

!IIII class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:28'!
/ unNumero
	unNumero	ifGreaterThan: self eval: [ self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor ].
	self ifGreaterThan: unNumero eval: [ ^ self - unNumero / unNumero + I ].
	^ I.! !

!IIII class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:28'!
next
	next ifNil: [
		next := self cloneNamed: (self name, 'I').
		next previous: self.
	].
	^ next! !

!IIII class methodsFor: 'operations' stamp: 'JP 3/29/2018 03:50:28'!
previous
	^ previous! !


!IIII class methodsFor: 'conditionals' stamp: 'JP 3/29/2018 03:50:28'!
ifGreaterThan: unNumero eval: unaClausura
	unNumero ifOne: unaClausura.
	previous ifGreaterThan: unNumero previous eval: unaClausura.! !

!IIII class methodsFor: 'conditionals' stamp: 'JP 3/29/2018 03:50:28'!
ifOne: unaClausura! !


!IIII class methodsFor: 'errors' stamp: 'JP 3/29/2018 03:50:28'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
	^'No se puede dividir por un numero mayor'! !


!IIII class methodsFor: 'class initialization' stamp: 'scn 4/1/2018 23:36:56'!
initialize
	"Inicialización de colaboradores internos next y previous."

	next := nil.
	previous := III.! !


!IIII class methodsFor: 'other' stamp: 'JP 3/29/2018 03:50:28'!
previous: aNumber
	previous := aNumber! !

!IIII class methodsFor: 'other' stamp: 'JP 3/29/2018 03:50:28'!
removeAllNext
	next ifNotNil: [
		next removeAllNext.
		next removeFromSystem.
		next := nil.
	]! !

!IIII class methodsFor: 'other' stamp: 'JP 3/29/2018 03:50:28'!
subtractTo: aNumber
	^ previous subtractTo: aNumber previous.! !

IIII instVarNamed: 'next' put: nil!
IIII instVarNamed: 'previous' put: III!
II initialize!
III initialize!
IIII initialize!