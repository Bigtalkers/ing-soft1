Números de Peano
Analice la definición axiomática de Peano de los números naturales.
Si lo necesita busque un libro en biblioteca o navegue por Internet.
Lea los axiomas. E intente representar los números de Peano con objetos.
Llamará al primer número `I`, al segundo número `II`, al tercero `III`,
al cuarto `IIII`, al cinco `IIIII` y así sucesivamente.
Inicialmente, concentrense en representar el `I` y el `II`. Vayan definiendo
los métodos necesarios para que los números de Peano por usted definidos sepan
responder los mensajes: `previous`, `next`, `+ unNumeroDePeano`,
`- unNumeroDePeano`, `* unNumeroDePeano`, `/ unNumeroDePeano`, en ese orden.
Dejen la división para el final que es más dificil. Cuando al `II` se le envíe
el mensaje `next`, automáticamente se debe crear el `III` y así sucesivamente.
Cuando se multipliquen números también automáticamente se deberán crear los
números que esa multiplicación abarque, entre ellos el resultado (por
ejemplo `II * II` generará el `III` y el `IIII` si aún no están representados y
retornará el `IIII`). Para la división, puede definirla de modo que retorne la
parte natural (`IIII/III` retorna `I`) o bien que solo funcione para divisiones
de resultado natural y cuando se pretende dividir números que no se dividen
haya un error (ej `IIII/III` genera un error).

Recuerde que en los métodos `*`, `+` y `-` no debe haber ningún `if`. Asegúrese
también de cumplir con el enunciado del ejercicio, para ello le adjuntamos
un conjunto de tests que deben pasar (haga file-in del archivo y asegúrese
antes que en su ambiente existan al menos los objetos `I`, `II`, `III` y `IIII`.
Para que pasen los tests 05 y 12 tendrá que realizar un *extract method* de
los mensajes de error que nombrará
`descripcionDeErrorDeNumerosNegativosNoSoportados` y
`descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor` ).