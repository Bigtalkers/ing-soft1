!classDefinition: #OOStackTest category: #'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'Something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:31'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'Something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/8/2012 08:20'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'Something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:33'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'First'.
	secondPushedObject := 'Second'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:35'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'Something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:36'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'Something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:36'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'Something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #OOStack category: #'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'theStackTop'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'accessing' stamp: 'SCN 4/21/2018 17:03:54'!
isEmpty
    ^theStackTop isEmpty.! !

!OOStack methodsFor: 'accessing' stamp: 'SCN 4/25/2018 19:01:30'!
size
    |sizeOfSelf|
    sizeOfSelf _ 0.
    theStackTop repeatforEachElement: [:elem | sizeOfSelf _ sizeOfSelf + 1].
    ^sizeOfSelf .! !

!OOStack methodsFor: 'accessing' stamp: 'SCN 4/25/2018 18:51:14'!
top
	^theStackTop topOtherwiseHandle: [self error: self class stackEmptyErrorDescription].! !


!OOStack methodsFor: 'associating' stamp: 'SCN 4/25/2018 18:51:36'!
pop
     |top|
     top _ self top.
     theStackTop _ theStackTop withoutTheTop.
	^top.! !

!OOStack methodsFor: 'associating' stamp: 'SCN 4/25/2018 18:54:32'!
push: something
	theStackTop _ theStackTop under: something.! !


!OOStack methodsFor: 'initialization' stamp: 'SCN 4/21/2018 17:03:48'!
initialize
    theStackTop _ OOStackEmpty new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: #'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'HernanWilkinson 5/7/2012 11:51'!
stackEmptyErrorDescription
	
	^ 'Stack is empty'! !


!classDefinition: #OOStackTop category: #'Stack-Exercise'!
Object subclass: #OOStackTop
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTop methodsFor: 'association' stamp: 'SCN 4/25/2018 18:16:47'!
under: something
     "Este m�todo no muta ning�n objeto. Por eso evitamos usar verbos en el nombre del mensaje."
	"Lo implementamos ac� para evitar repetir c�digo."
	^ OOStackElement new
		initializeWith: something
		over: self.! !


!OOStackTop methodsFor: 'accessing' stamp: 'SCN 4/21/2018 17:11:31'!
isEmpty
    self subclassResponsibility.! !

!OOStackTop methodsFor: 'accessing' stamp: 'SCN 4/25/2018 18:28:38'!
topOtherwiseHandle: aBlock
	self subclassResponsibility.! !

!OOStackTop methodsFor: 'accessing' stamp: 'SCN 4/25/2018 18:25:31'!
withoutTheTop
	self subclassResponsibility.! !


!OOStackTop methodsFor: 'evaluating' stamp: 'SCN 4/21/2018 17:51:57'!
repeatforEachElement: aBlock
	self subclassResponsibility.! !


!classDefinition: #OOStackElement category: #'Stack-Exercise'!
OOStackTop subclass: #OOStackElement
	instanceVariableNames: 'elem stack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackElement methodsFor: 'initialization' stamp: 'SCN 4/21/2018 16:56:23'!
initializeWith: anElement over: anOOStackTop
	elem _ anElement.
	stack _ anOOStackTop.! !


!OOStackElement methodsFor: 'accessing' stamp: 'SCN 4/21/2018 16:27:09'!
isEmpty
    ^false.! !

!OOStackElement methodsFor: 'accessing' stamp: 'SCN 4/25/2018 18:28:38'!
topOtherwiseHandle: aBlock
	^ elem.! !

!OOStackElement methodsFor: 'accessing' stamp: 'SCN 4/25/2018 18:25:31'!
withoutTheTop
	^ stack.! !


!OOStackElement methodsFor: 'evaluating' stamp: 'SCN 4/25/2018 19:08:44'!
repeatforEachElement: aBlock
     "El bloque no tenemos por qu� evaluarlo con el elemento de este objeto pero consideramos que es un feature lindo y sencillo de implementar - Bigtalkers."
	aBlock value: elem.
	stack repeatforEachElement: aBlock.! !


!classDefinition: #OOStackEmpty category: #'Stack-Exercise'!
OOStackTop subclass: #OOStackEmpty
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackEmpty methodsFor: 'accessing' stamp: 'SCN 4/25/2018 11:44:57'!
isEmpty
    ^true.! !

!OOStackEmpty methodsFor: 'accessing' stamp: 'SCN 4/25/2018 18:28:38'!
topOtherwiseHandle: aBlock
	aBlock value.! !



!OOStackEmpty methodsFor: 'evaluating' stamp: 'SCN 4/21/2018 17:51:57'!
repeatforEachElement: aBlock
	"Termina la iteraci�n."
	^ self.! !
