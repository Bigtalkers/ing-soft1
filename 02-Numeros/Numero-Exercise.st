!classDefinition: #NumeroTest category: #'Numero-Exercise'!
TestCase subclass: #NumeroTest
	instanceVariableNames: 'zero one two four oneFifth oneHalf five twoFifth twoTwentyfifth fiveHalfs'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:11'!
test01isCeroReturnsTrueWhenAskToZero

	self assert: zero isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:12'!
test02isCeroReturnsFalseWhenAskToOthersButZero

	self deny: one isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test03isOneReturnsTrueWhenAskToOne

	self assert: one isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test04isOneReturnsFalseWhenAskToOtherThanOne

	self deny: zero isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:14'!
test05EnteroAddsWithEnteroCorrectly

	self assert: one + one equals: two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:18'!
test06EnteroMultipliesWithEnteroCorrectly

	self assert: two * two equals: four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:20'!
test07EnteroDividesEnteroCorrectly

	self assert: two / two equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:38'!
test08FraccionAddsWithFraccionCorrectly
"
    La suma de fracciones es:
	 
	a/b + c/d = (a.d + c.b) / (b.d)
	 
	SI ESTAN PENSANDO EN LA REDUCCION DE FRACCIONES NO SE PREOCUPEN!!
	TODAVIA NO SE ESTA TESTEANDO ESE CASO
"
	| sevenTenths |

	sevenTenths := (Entero with: 7) / (Entero with: 10).

	self assert: oneFifth + oneHalf equals: sevenTenths! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:52'!
test09FraccionMultipliesWithFraccionCorrectly
"
    La multiplicacion de fracciones es:
	 
	(a/b) * (c/d) = (a.c) / (b.d)
"

	self assert: oneFifth * twoFifth equals: twoTwentyfifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:56'!
test10FraccionDividesFraccionCorrectly
"
    La division de fracciones es:
	 
	(a/b) / (c/d) = (a.d) / (b.c)
"

	self assert: oneHalf / oneFifth equals: fiveHalfs! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test11EnteroAddsFraccionCorrectly
"
	Ahora empieza la diversion!!
"

	self assert: one + oneFifth equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test12FraccionAddsEnteroCorrectly

	self assert: oneFifth + one equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:50'!
test13EnteroMultipliesFraccionCorrectly

	self assert: two * oneFifth equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:52'!
test14FraccionMultipliesEnteroCorrectly

	self assert: oneFifth * two equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:57'!
test15EnteroDividesFraccionCorrectly

	self assert: one / twoFifth equals: fiveHalfs  ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:59'!
test16FraccionDividesEnteroCorrectly

	self assert: twoFifth / five equals: twoTwentyfifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:38'!
test17AFraccionCanBeEqualToAnEntero

	self assert: two equals: four / two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:39'!
test18AparentFraccionesAreEqual

	self assert: oneHalf equals: two / four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:40'!
test19AddingFraccionesCanReturnAnEntero

	self assert: oneHalf + oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test20MultiplyingFraccionesCanReturnAnEntero

	self assert: (two/five) * (five/two) equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test21DividingFraccionesCanReturnAnEntero

	self assert: oneHalf / oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:43'!
test22DividingEnterosCanReturnAFraccion

	self assert: two / four equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test23CanNotDivideEnteroByZero

	self 
		should: [ one / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test24CanNotDivideFraccionByZero

	self 
		should: [ oneHalf / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test25AFraccionCanNotBeZero

	self deny: oneHalf isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test26AFraccionCanNotBeOne

	self deny: oneHalf isOne! !


!NumeroTest methodsFor: 'setup' stamp: 'HernanWilkinson 5/7/2016 20:56'!
setUp

	zero := Entero with: 0.
	one := Entero with: 1.
	two := Entero with: 2.
	four := Entero with: 4.
	five := Entero with: 5.
	
	oneHalf := one / two.
	oneFifth := one / five.
	twoFifth := two / five.
	twoTwentyfifth := two / (Entero with: 25).
	fiveHalfs := five / two.
	! !


!classDefinition: #Numero category: #'Numero-Exercise'!
Object subclass: #Numero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
* aMultiplier

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
+ anAdder

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
/ aDivisor

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 21:21:51'!
dividirAEntero: unEntero
	self subclassResponsibility! !

!Numero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 21:22:26'!
dividirAFraccion: unaFraccion
	self subclassResponsibility! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
invalidNumberType

	self error: self class invalidNumberTypeErrorDescription! !

!Numero methodsFor: 'arithmetic operations' stamp: 'JOP 4/22/2018 00:42:29'!
inverse
	self subclassResponsibility! !

!Numero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 21:23:18'!
multiplicarAEntero: unEntero
	self subclassResponsibility! !

!Numero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 21:23:38'!
multiplicarAFraccion: unaFraccion
	self subclassResponsibility! !

!Numero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 21:23:24'!
sumarAEntero: unEntero
	self subclassResponsibility! !

!Numero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 21:23:34'!
sumarAFraccion: unaFraccion
	self subclassResponsibility! !

!Numero methodsFor: 'arithmetic operations' stamp: 'JOP 4/22/2018 00:43:23'!
sumarUno
	self subclassResponsibility! !


!Numero methodsFor: 'testing' stamp: 'JOP 4/22/2018 00:40:17'!
isOne
	^self isKindOf: Uno! !

!Numero methodsFor: 'testing' stamp: 'JOP 4/22/2018 00:40:21'!
isZero
	^self isKindOf: Cero! !


!Numero methodsFor: 'comparing' stamp: 'JOP 4/22/2018 01:41:59'!
= aNumber
	self subclassResponsibility ! !

!Numero methodsFor: 'comparing' stamp: 'JOP 4/22/2018 01:42:04'!
hash
	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Numero class' category: #'Numero-Exercise'!
Numero class
	instanceVariableNames: ''!

!Numero class methodsFor: 'error descriptions' stamp: 'HernanWilkinson 5/7/2016 22:45'!
canNotDivideByZeroErrorDescription

	^'No se puede dividir por cero'! !

!Numero class methodsFor: 'error descriptions' stamp: 'HernanWilkinson 5/7/2016 22:47'!
invalidNumberTypeErrorDescription
	^ 'Tipo de numero invalido'! !


!classDefinition: #Cero category: #'Numero-Exercise'!
Numero subclass: #Cero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Cero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 20:13:44'!
* aMultiplier
	^self! !

!Cero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 20:12:42'!
+ anAdder
	^anAdder! !

!Cero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 21:16:33'!
/ aDivisor
	^self! !

!Cero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 20:25:13'!
dividirAEntero: unEntero
	self error: Numero canNotDivideByZeroErrorDescription ! !

!Cero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 20:25:31'!
dividirAFraccion: unaFraccion
	self error: Numero canNotDivideByZeroErrorDescription ! !

!Cero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 23:45:11'!
divisionEnteraAEntero: unEntero
	self error: Numero canNotDivideByZeroErrorDescription ! !

!Cero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 21:43:21'!
inverse
	self error: Numero canNotDivideByZeroErrorDescription ! !

!Cero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 20:26:12'!
multiplicarAEntero: unEntero
	^self! !

!Cero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 20:26:25'!
multiplicarAFraccion: unaFraccion
	^self! !

!Cero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 20:25:59'!
sumarAEntero: unEntero
	^unEntero! !

!Cero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 20:26:33'!
sumarAFraccion: unaFraccion
	^unaFraccion! !

!Cero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 22:07:53'!
sumarUno
	^Uno new! !


!Cero methodsFor: 'printing' stamp: 'as 4/21/2018 19:27:58'!
printOn: aStream
	aStream nextPutAll: '0'! !


!Cero methodsFor: 'comparing' stamp: 'JOP 4/21/2018 23:57:38'!
= aCero
	^aCero isKindOf: self class! !

!Cero methodsFor: 'comparing' stamp: 'JOP 4/22/2018 01:42:20'!
hash
	^0 hash! !


!classDefinition: #Entero category: #'Numero-Exercise'!
Numero subclass: #Entero
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Entero methodsFor: 'value' stamp: 'HernanWilkinson 5/7/2016 21:02'!
integerValue

	"Usamos integerValue en vez de value para que no haya problemas con el mensaje value implementado en Object - Hernan"
	
	^value! !


!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 21:01'!
= anObject

	^(anObject isKindOf: self class) and: [ value = anObject integerValue ]! !

!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:17'!
hash

	^value hash! !


!Entero methodsFor: 'initialization' stamp: 'as 4/21/2018 19:36:01'!
initializeWith: aValue 
	
	value := aValue! !


!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 20:27:16'!
* aMultiplier 
	^ aMultiplier multiplicarAEntero: self! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 20:37:34'!
+ anAdder
	^ anAdder sumarAEntero: self.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 20:38:11'!
/ aDivisor 
	^ aDivisor dividirAEntero: self! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 23:53:34'!
// aDivisor 
	^aDivisor divisionEnteraAEntero: self! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/22/2018 00:12:37'!
dividirAEntero: unEntero
	| greatestCommonDivisor numerator denominator |
		
	greatestCommonDivisor := self greatestCommonDivisorWith: unEntero.
	numerator := unEntero // greatestCommonDivisor.
	denominator := self // greatestCommonDivisor.

	^denominator under: numerator! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 20:23:37'!
dividirAFraccion: unaFraccion
	^ unaFraccion numerator / (unaFraccion denominator * self)! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 23:42:59'!
divisionEnteraAEntero: aDivisor
	^self class with: aDivisor integerValue // value! !

!Entero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 21:00'!
greatestCommonDivisorWith: anEntero 
	
	^self class with: (value gcd: anEntero integerValue)! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/22/2018 00:46:07'!
inverse
	^self under: Uno new! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 20:27:48'!
multiplicarAEntero: unEntero
	^self class with: value * unEntero integerValue! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 20:29:00'!
multiplicarAFraccion: unaFraccion
	^ self * unaFraccion numerator / unaFraccion denominator! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 20:24:57'!
sumarAEntero: unEntero
	^ self class with: value + unEntero integerValue.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 20:25:24'!
sumarAFraccion: unaFraccion
	^ self * unaFraccion denominator + unaFraccion numerator / unaFraccion denominator.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 22:12:13'!
sumarUno
	^self class with: value + 1! !


!Entero methodsFor: 'private' stamp: 'JOP 4/22/2018 00:52:50'!
under: aNumerator
	"Solo para uso interno de la clase"
	^Fraccion new initializeWith: aNumerator over: self! !


!Entero methodsFor: 'printing' stamp: 'JOP 4/19/2018 20:36:58'!
printOn: aStream
	aStream nextPutAll: value printString! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Entero class' category: #'Numero-Exercise'!
Entero class
	instanceVariableNames: ''!

!Entero class methodsFor: 'instance creation' stamp: 'JOP 4/21/2018 21:37:51'!
with: aValue 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].
	
	"No podemos modificar la clase Integer ni los tests asi que dejamos estos if - BigTalkers"
     aValue = 0 ifTrue: [^Cero new].
	aValue = 1 ifTrue: [^Uno new].
	^self new initializeWith: aValue! !


!classDefinition: #Fraccion category: #'Numero-Exercise'!
Numero subclass: #Fraccion
	instanceVariableNames: 'numerator denominator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Fraccion methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 22:54'!
initializeWith: aNumerator over: aDenominator

	"Estas precondiciones estan por si se comenten errores en la implementacion - Hernan"
	aNumerator isZero ifTrue: [ self error: 'una fraccion no puede ser cero' ].
	aDenominator isOne ifTrue: [ self error: 'una fraccion no puede tener denominador 1 porque sino es un entero' ].
	
	numerator := aNumerator.
	denominator := aDenominator ! !


!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:42'!
= anObject

	^(anObject isKindOf: self class) and: [ (numerator * anObject denominator) = (denominator * anObject numerator) ]! !

!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:50'!
hash

	^(numerator hash / denominator hash) hash! !


!Fraccion methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 20:47:25'!
* aMultiplier 
	^ aMultiplier multiplicarAFraccion: self! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 21:04:21'!
+ anAdder
	^ anAdder sumarAFraccion: self.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 21:13:10'!
/ aDivisor 
	^ aDivisor dividirAFraccion: self! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 21:03:05'!
dividirAEntero: unEntero
	^ unEntero * denominator / numerator! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 21:02:28'!
dividirAFraccion: unaFraccion
		^ (unaFraccion numerator * denominator) / (unaFraccion denominator * numerator)! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'JOP 4/22/2018 00:11:13'!
inverse
	^denominator / numerator! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 20:45:29'!
multiplicarAEntero: unEntero
	^numerator * unEntero / denominator! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 20:47:02'!
multiplicarAFraccion: unaFraccion
		^(numerator * unaFraccion numerator) / (denominator * unaFraccion denominator)! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 21:14:07'!
sumarAEntero: unEntero
	^ denominator * unEntero + numerator / denominator.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'JOP 4/19/2018 21:04:21'!
sumarAFraccion: unaFraccion
	^ numerator * unaFraccion denominator + (denominator * unaFraccion numerator) / (denominator * unaFraccion denominator).! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'JOP 4/22/2018 00:03:58'!
sumarUno
	"Suma de coprimos es coprimo con los operandos"
	^self class new initializeWith: denominator + numerator over: denominator! !


!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
denominator

	^ denominator! !

!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
numerator

	^ numerator! !


!Fraccion methodsFor: 'printing' stamp: 'JOP 4/19/2018 20:43:14'!
printOn: aStream
    aStream nextPutAll: numerator printString, ' / ', denominator printString! !


!classDefinition: #Uno category: #'Numero-Exercise'!
Numero subclass: #Uno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Uno methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 21:40:40'!
* aMultiplier
	^aMultiplier! !

!Uno methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 23:48:30'!
+ anAdder
	^anAdder sumarUno! !

!Uno methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 21:40:25'!
/ aDivisor
	^aDivisor inverse! !

!Uno methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 22:16:19'!
dividirAEntero: unEntero
	^unEntero! !

!Uno methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 22:16:31'!
dividirAFraccion: unaFraccion
	^unaFraccion! !

!Uno methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 23:43:39'!
divisionEnteraAEntero: unEntero
	^unEntero! !

!Uno methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 21:40:59'!
inverse
	^self! !

!Uno methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 22:16:06'!
multiplicarAEntero: unEntero
	^unEntero! !

!Uno methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 22:15:55'!
multiplicarAFraccion: unaFraccion
	^unaFraccion! !

!Uno methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 22:13:19'!
sumarAEntero: unEntero
	^unEntero sumarUno! !

!Uno methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 22:15:20'!
sumarAFraccion: unaFraccion
	^unaFraccion sumarUno! !

!Uno methodsFor: 'arithmetic operations' stamp: 'JOP 4/21/2018 22:07:06'!
sumarUno
	^Entero with: 2! !


!Uno methodsFor: 'comparing' stamp: 'JOP 4/21/2018 23:58:40'!
= aUno
	^aUno isKindOf: self class! !

!Uno methodsFor: 'comparing' stamp: 'JOP 4/22/2018 01:42:31'!
hash
	^1 hash! !


!Uno methodsFor: 'printing' stamp: 'JOP 4/22/2018 00:10:21'!
printOn: aStream
	aStream nextPutAll: '1'! !


!Uno methodsFor: 'private' stamp: 'JOP 4/22/2018 00:19:06'!
under: aNumerator
	^aNumerator! !
