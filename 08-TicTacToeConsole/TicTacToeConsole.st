!classDefinition: #TicTacToeTest category: #TicTacToe!
TestCase subclass: #TicTacToeTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'SCN 5/31/2018 14:56:29'!
test01GameStartsEmpty

	|game|
	
	game := TicTacToe new.
	
	self assert: game Xs isEmpty.
	self assert: game Os isEmpty.
	self assert: game XIsPlaying.
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'SCN 5/31/2018 14:56:47'!
test02XCanStartCorrectly
	| game |
	game _ TicTacToe new.
	game putXAt: 1 @ 1.
	self
		assert: 1
		equals: game Xs size.
	self assert: (game Xs includes: 1 @ 1).
	self assert: game Os isEmpty.
	self assert: game OIsPlaying.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:40:32'!
test03OCanFollowCorrectly

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	
	self assert: 1 equals: game Xs size.
	self assert: (game Xs includes: 1@1).
	self assert: 1 equals: game Os size.
	self assert: (game Os includes: 2@2).
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:40:54'!
test04XCannotPlayTwiceInARow

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	
	self
	should: [game putXAt: 2@2]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe notXTurnErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: game Os isEmpty.
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:41:20'!
test05OCannotPlayTwiceInARow

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	self
	should: [game putOAt: 3@3]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe notOTurnErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: 1 equals: game Os size.
		self assert: (game Os includes: 2@2).
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:42:32'!
test06XCannotPlayOnAnotherX
	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	self
	should: [game putXAt: 1@1]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe positionTakenErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: 1 equals: game Os size.
		self assert: (game Os includes: 2@2).
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:42:41'!
test07XCannotPlayOnAnotherO

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	self
	should: [game putXAt: 2@2]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe positionTakenErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: 1 equals: game Os size.
		self assert: (game Os includes: 2@2).
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:42:53'!
test08OCannotPlayOnAnotherX

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	self
	should: [game putOAt: 1@1]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe positionTakenErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert:  game Os isEmpty.

		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:43:05'!
test09OCannotPlayOnAnotherO

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	game putXAt: 3@3.
	self
	should: [game putOAt: 2@2]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe positionTakenErrorMessage equals: anError messageText.	
		self assert: 2 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: (game Xs includes: 3@3).
		self assert: 1 equals: game Os size.
		self assert: (game Os includes: 2@2).
		
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:43:38'!
test10PlayersShouldNotWinIfConditionsAreNotMet

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@1.
	game putXAt: 2@2.
	game putOAt: 3@3.
	
		self deny: game OHasWon.
		self deny: game XHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:44:57'!
test11XShouldWinIfCompletedFirstRow

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@1.
	game putXAt: 1@2.
	game putOAt: 3@3.
	game putXAt: 1@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:45:10'!
test12XShouldWinIfCompletedSecondRow

	|game|
	
	game := TicTacToe new.

	game putXAt: 2@1.
	game putOAt: 1@1.
	game putXAt: 2@2.
	game putOAt: 3@1.
	game putXAt: 2@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:45:21'!
test13XShouldWinIfCompletedThirdRow

	|game|
	
	game := TicTacToe new.

	game putXAt: 3@1.
	game putOAt: 1@1.
	game putXAt: 3@2.
	game putOAt: 2@1.
	game putXAt: 3@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:45:37'!
test14XShouldWinIfCompletedAColumn

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	game putXAt: 2@1.
	game putOAt: 3@3.
	game putXAt: 3@1.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:45:50'!
test15XShouldWinIfCompletedDownDiagonal

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@1.
	game putXAt: 2@2.
	game putOAt: 2@3.
	game putXAt: 3@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:46:07'!
test16XShouldWinIfCompletedUpDiagonal

	|game|
	
	game := TicTacToe new.

	game putXAt: 3@1.
	game putOAt: 2@1.
	game putXAt: 2@2.
	game putOAt: 1@2.
	game putXAt: 1@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:46:26'!
test17OShouldWinIfCompletedUpDiagonal

	|game|
	
	game := TicTacToe new.

	game putXAt: 3@1.
	game putOAt: 1@1.
	game putXAt: 2@2.
	game putOAt: 1@2.
	game putXAt: 3@3.
	game putOAt: 1@3.
	
		self assert: game OHasWon.
		self deny: game XHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:47:37'!
test18OShouldNotBeAbleToPlayIfGameOver

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 1@1.	
	game putOAt: 2@1.
	game putXAt: 1@2.
	game putOAt: 3@1.	
	game putXAt: 1@3.

	self 
		should: [ game putOAt: 3@1 ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: TicTacToe canNotPlayWhenGameIsOverErrorMessage equals: anError messageText.
			self assert: 2 equals: game Os size.
			self assert: (game Os includes: 2@1).
			self assert: (game Os includes: 3@1) ]
			
			! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:47:43'!
test19XShouldNotBeAbleToPlayIfGameOver

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 2@2.
	game putOAt: 1@1.	
	game putXAt: 2@1.
	game putOAt: 1@2.
	game putXAt: 3@1.	
	game putOAt: 1@3.

	self 
		should: [ game putXAt: 3@2 ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: TicTacToe canNotPlayWhenGameIsOverErrorMessage equals: anError messageText.
			self assert: 3 equals: game Xs size.
			self assert: (game Xs includes: 2@2).
			self assert: (game Xs includes: 2@1).
			self assert: (game Xs includes: 3@1) ]
			
			! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:48:35'!
test20GameShouldBeTiedIfNoMovesArePossibleAndNobodyWon

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 2@2.
	game putOAt: 1@1.	
	game putXAt: 1@3.
	game putOAt: 3@1.
	game putXAt: 2@1.	
	game putOAt: 2@3.
	game putXAt: 1@2.	
	game putOAt: 3@2.
	game putXAt: 3@3.
	
	self assert: game isOver.
	self deny: game XHasWon. 
	self deny: game OHasWon. 
	self assert: game isTied
! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 5/31/2018 05:49:08'!
test21XShouldWinEvenIfNoMovesArePossibleIfConditionsAreMet

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 2@2.
	game putOAt: 1@2.	
	game putXAt: 1@1.
	game putOAt: 2@1.
	game putXAt: 1@3.	
	game putOAt: 2@3.
	game putXAt: 3@2.	
	game putOAt: 3@1.
	game putXAt: 3@3.
	
	self assert: game isOver.
	self assert: game XHasWon. 
	self deny: game OHasWon. 
	self deny: game isTied
! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'SCN 5/31/2018 14:57:24'!
test22NobodyShouldBePlayingAtGameOver
	| game |
	game _ TicTacToe new.
	game putXAt: 1 @ 1.
	game putOAt: 2 @ 1.
	game putXAt: 1 @ 2.
	game putOAt: 3 @ 3.
	game putXAt: 1 @ 3.
	self deny: game XIsPlaying.
	self deny: game OIsPlaying.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 15:16:26'!
test23ShouldAcceptACallbackOnPlayX
	| game firstPlay |
	game _ TicTacToe new.
	game onPlay: [:play :state | firstPlay := play].
	game putXAt: 2@2.
	self assert: (firstPlay player = 'X').
	self assert: (firstPlay position = (2@2)).! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 15:16:34'!
test24ShouldAcceptACallbackOnPlayO
	| game secondPlay |
	game _ TicTacToe new.
	game onPlay: [:play :state | secondPlay := play].
	game putXAt: 2@2.
	game putOAt: 1@1.
	self assert: (secondPlay player = 'O').
	self assert: (secondPlay position = (1@1)).! !


!classDefinition: #TicTacToeViewerHubTest category: #TicTacToe!
TestCase subclass: #TicTacToeViewerHubTest
	instanceVariableNames: 'tictactoeXwins loggerXwins hubXwins tictactoeOwins loggerOwins hubOwins tictactoetied loggertied hubtied viewXwins viewOwins viewtied'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeViewerHubTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 17:02:17'!
test01LogShouldShowXPlay
	| tictactoe logger hub |
	tictactoe _ TicTacToe new.
	logger _ TicTacToeLogger new.
	hub _ TicTacToeViewerHub view: tictactoe.
	hub attach: logger.
	tictactoe putXAt: 2@2.
	self assert: ((logger at: 1) = 'X marc� en 2@2').
	self assert: ((logger at: 2) = 'Estado: Jugando O').! !

!TicTacToeViewerHubTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 17:02:21'!
test02LogShouldShowOPlay
	| tictactoe logger hub |
	tictactoe _ TicTacToe new.
	logger _ TicTacToeLogger new.
	hub _ TicTacToeViewerHub view: tictactoe.
	hub attach: logger.
	tictactoe putXAt: 2@2.
	tictactoe putOAt: 1@1.
	self assert: ((logger at: 3) = 'O marc� en 1@1').
	self assert: ((logger at: 4) = 'Estado: Jugando X').! !

!TicTacToeViewerHubTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 17:02:27'!
test03LogShouldShowXWin
	self assert: ((loggerXwins at: 10) = 'Estado: Juego terminado - Gan� X!!').! !

!TicTacToeViewerHubTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 17:02:38'!
test04LogShouldShowOWin
	self assert: ((loggerOwins at: 12) = 'Estado: Juego terminado - Gan� O!!').! !

!TicTacToeViewerHubTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 17:02:46'!
test05LogShouldShowTie
	self assert: ((loggertied at: 18) = 'Estado: Juego terminado - Empataron').! !

!TicTacToeViewerHubTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 17:02:54'!
test06ViewShouldShowXPlay
	| tictactoe view hub |
	tictactoe _ TicTacToe new.
	view _ TicTacToeView new.
	hub _ TicTacToeViewerHub view: tictactoe.
	
	hub attach: view.
	tictactoe putXAt: 2@2.
	self assert: view asString = 
' | | 
-----
 |X| 
-----
 | | 
Jugando O'! !

!TicTacToeViewerHubTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 17:03:00'!
test07ViewShouldShowOPlay
	| tictactoe view hub |
	tictactoe _ TicTacToe new.
	view _ TicTacToeView new.
	hub _ TicTacToeViewerHub view: tictactoe.
	
	hub attach: view.
	tictactoe putXAt: 2@2.
	tictactoe putOAt: 1@1.
	self assert: view asString = 
'O| | 
-----
 |X| 
-----
 | | 
Jugando X'! !

!TicTacToeViewerHubTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 17:03:07'!
test08ViewShouldShowXWin
	self assert: viewXwins asString = 
'O| | 
-----
X|X|X
-----
 | |O
Juego terminado - Gan� X!!'! !

!TicTacToeViewerHubTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 17:03:12'!
test09ViewShouldShowOWin
	self assert: viewOwins asString = 
'O| | 
-----
O|X|X
-----
O|X| 
Juego terminado - Gan� O!!'! !

!TicTacToeViewerHubTest methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 17:03:18'!
test10ViewShouldShowTie
	self assert: viewtied asString = 
'O|O|X
-----
X|X|O
-----
O|X|X
Juego terminado - Empataron'! !


!TicTacToeViewerHubTest methodsFor: 'test setup' stamp: 'SCN 6/4/2018 16:54:33'!
setUp

	tictactoeXwins _ TicTacToe new.
	loggerXwins _ TicTacToeLogger new.
	viewXwins _ TicTacToeView new.
	hubXwins _ TicTacToeViewerHub view: tictactoeXwins.
	hubXwins attach: loggerXwins.
	hubXwins attach: viewXwins.
	tictactoeXwins putXAt: 2@2.
	tictactoeXwins putOAt: 1@1.
	tictactoeXwins putXAt: 1@2.
	tictactoeXwins putOAt: 3@3.
	tictactoeXwins putXAt: 3@2.
	
	tictactoeOwins _ TicTacToe new.
	loggerOwins _ TicTacToeLogger new.
	viewOwins _ TicTacToeView new.
	hubOwins _ TicTacToeViewerHub view: tictactoeOwins.
	hubOwins attach: loggerOwins.
	hubOwins attach: viewOwins.
	tictactoeOwins putXAt: 2@2.
	tictactoeOwins putOAt: 1@1.
	tictactoeOwins putXAt: 2@3.
	tictactoeOwins putOAt: 1@3.
	tictactoeOwins putXAt: 3@2.
	tictactoeOwins putOAt: 1@2.
	
	tictactoetied _ TicTacToe new.
	loggertied _ TicTacToeLogger new.
	viewtied _ TicTacToeView new.
	hubtied _ TicTacToeViewerHub view: tictactoetied.
	hubtied attach: loggertied.
	hubtied attach: viewtied.
	tictactoetied putXAt: 2@2.
	tictactoetied putOAt: 1@1.
	tictactoetied putXAt: 2@3.
	tictactoetied putOAt: 1@3.
	tictactoetied putXAt: 1@2.
	tictactoetied putOAt: 3@2.
	tictactoetied putXAt: 3@3.
	tictactoetied putOAt: 2@1.
	tictactoetied putXAt: 3@1.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TicTacToeViewerHubTest class' category: #TicTacToe!
TicTacToeViewerHubTest class
	instanceVariableNames: ''!

!TicTacToeViewerHubTest class methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 14:30:41'!
test01
	| tictactoe |
	tictactoe _ TicTacToe new.
	TicTacToeViewerHub view: tictactoe.! !

!TicTacToeViewerHubTest class methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 14:32:09'!
test02
	| tictactoe logger |
	tictactoe _ TicTacToe new.
	logger _ TicTacToeLogger new.
	TicTacToeViewerHub view: tictactoe.
	TicTacToeViewerHub attach: logger.! !


!classDefinition: #TicTacToe category: #TicTacToe!
Object subclass: #TicTacToe
	instanceVariableNames: 'os xs state onPlayClosure'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToe methodsFor: 'checking' stamp: 'JOP 5/31/2018 12:12:30'!
hasCompletedColumn: positions
	
	^ (1 to: 3) anySatisfy: [ :y | (positions count: [ :position | position y = y ]) = 3 ]! !

!TicTacToe methodsFor: 'checking' stamp: 'JOP 5/31/2018 12:12:41'!
hasCompletedRow: positions

	^ (1 to: 3) anySatisfy: [ :x | (positions count: [ :posicion | posicion x = x ]) = 3 ]! !

!TicTacToe methodsFor: 'checking' stamp: 'JOP 5/31/2018 12:13:11'!
hasDownDiagonal: positions

	^(1 to: 3) allSatisfy: [ :n | positions includes: n@n ]
! !

!TicTacToe methodsFor: 'checking' stamp: 'JOP 5/31/2018 12:13:21'!
hasUpDiagonal: positions

	^(1 to: 3) allSatisfy: [ :n | positions includes: n@(4-n) ]! !

!TicTacToe methodsFor: 'checking' stamp: 'JOP 5/31/2018 12:13:28'!
hasWin: positions

	^(self hasCompletedRow: positions)
		or: [(self hasCompletedColumn: positions) 
				or: [(self hasDownDiagonal: positions)
					or: [(self hasUpDiagonal: positions)]]]

! !


!TicTacToe methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:51:24'!
checkPositionIsFree: aPosition
	((xs includes: aPosition) or: (os includes: aPosition )) ifTrue: [ self error: self class positionTakenErrorMessage ].! !

!TicTacToe methodsFor: 'playing' stamp: 'JOP 6/4/2018 14:21:55'!
onPlay: aClosure
	onPlayClosure _ aClosure ! !

!TicTacToe methodsFor: 'playing' stamp: 'JOP 6/4/2018 14:48:56'!
putOAt: aPosition
	state := state putOWith: [self checkPositionIsFree: aPosition. os add: aPosition] checkWinWith: [self hasWin: os].
	onPlayClosure value: (TicTacToePlay player: 'O' at: aPosition) value: state.! !

!TicTacToe methodsFor: 'playing' stamp: 'JOP 6/4/2018 14:49:05'!
putXAt: aPosition
	state := state putXWith: [self checkPositionIsFree: aPosition. xs add: aPosition] checkWinWith: [self hasWin: xs].
	onPlayClosure value: (TicTacToePlay player: 'X' at: aPosition) value: state.! !


!TicTacToe methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:49:24'!
OHasWon
	^state OHasWon! !

!TicTacToe methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:49:31'!
OIsPlaying
	^ state OIsPlaying! !

!TicTacToe methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:49:40'!
XHasWon
	^ state XHasWon! !

!TicTacToe methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:49:36'!
XIsPlaying
	^ state XIsPlaying! !

!TicTacToe methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:49:53'!
isOver
	^ state isOver! !

!TicTacToe methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:49:59'!
isTied
	^state isTied! !


!TicTacToe methodsFor: 'accessing' stamp: 'jg 5/22/2018 10:01:34'!
Os

	^os copy.! !

!TicTacToe methodsFor: 'accessing' stamp: 'jg 5/22/2018 10:01:25'!
Xs

	^xs copy.! !


!TicTacToe methodsFor: 'initialization' stamp: 'JOP 6/4/2018 15:16:12'!
initialize
	xs _ Set new.
	os _ Set new.
	state _ TicTacToeStateTurnX atTurn: 1.
	onPlayClosure _ [:play :state].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TicTacToe class' category: #TicTacToe!
TicTacToe class
	instanceVariableNames: ''!

!TicTacToe class methodsFor: 'errors' stamp: 'JOP 5/31/2018 05:24:16'!
canNotPlayWhenGameIsOverErrorMessage
	
	^'Can not play when game is over'! !

!TicTacToe class methodsFor: 'errors' stamp: 'JOP 5/31/2018 05:23:55'!
notOTurnErrorMessage

	^'Not O turn'! !

!TicTacToe class methodsFor: 'errors' stamp: 'JOP 5/31/2018 05:22:57'!
notXTurnErrorMessage

	^'Not X turn'! !

!TicTacToe class methodsFor: 'errors' stamp: 'JOP 5/31/2018 05:24:28'!
positionTakenErrorMessage
	
	^'Position taken'! !



!classDefinition: #TicTacToeObserver category: #TicTacToe!
Object subclass: #TicTacToeObserver
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeObserver methodsFor: 'notify' stamp: 'JOP 6/4/2018 15:35:02'!
notify: aPlay onState: aState
	self subclassResponsibility ! !


!classDefinition: #TicTacToeLogger category: #TicTacToe!
TicTacToeObserver subclass: #TicTacToeLogger
	instanceVariableNames: 'log'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeLogger methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 14:59:57'!
at: anIndex
	^ log at: anIndex ! !

!TicTacToeLogger methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 14:59:13'!
initialize
	log _ OrderedCollection new.! !

!TicTacToeLogger methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 14:58:53'!
log: aString
	log add: aString! !

!TicTacToeLogger methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 15:09:37'!
notify: aPlay onState: aState
	self log: aPlay player, ' marc� en ', aPlay position asString.
	self log: 'Estado: ', aState asString! !


!classDefinition: #TicTacToeView category: #TicTacToe!
TicTacToeObserver subclass: #TicTacToeView
	instanceVariableNames: 'view canvas state'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeView methodsFor: 'printing' stamp: 'JOP 6/4/2018 16:16:17'!
printLine: aLineNumber on: aStream.
	aStream nextPutAll: (view i: aLineNumber j: 1).
	2 to: 3 do: [ :j | 
		aStream nextPutAll: '|'.
		aStream nextPutAll: (view i: aLineNumber j: j).
	].
	aStream nextPutAll: (String with: Character newLineCharacter).! !

!TicTacToeView methodsFor: 'printing' stamp: 'JOP 6/4/2018 16:21:23'!
printOn: aStream
	self printLine: 1 on: aStream.
	2 to: 3 do: [ :i | 
		aStream nextPutAll: '-----', (String with: Character newLineCharacter).
		self printLine: i on: aStream.
	].
	aStream nextPutAll: state.! !


!TicTacToeView methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 16:20:50'!
initialize
	view _ Array2D height: 3 width: 3.
	view replaceValues: [ :i :j :value | ' '].
	state _ ''.! !

!TicTacToeView methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 16:20:55'!
notify: aPlay onState: aState
	view at: aPlay position put: aPlay player.
	state _ aState asString.! !


!classDefinition: #TicTacToePlay category: #TicTacToe!
Object subclass: #TicTacToePlay
	instanceVariableNames: 'player position'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToePlay methodsFor: 'accessing' stamp: 'JOP 6/4/2018 13:35:06'!
player
	^player! !

!TicTacToePlay methodsFor: 'accessing' stamp: 'JOP 6/4/2018 13:35:16'!
position
	^position! !


!TicTacToePlay methodsFor: 'initialization' stamp: 'JOP 6/4/2018 13:04:46'!
initializeWithPlayer: aPlayer at: aPosition
	player _ aPlayer.
	position _ aPosition.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TicTacToePlay class' category: #TicTacToe!
TicTacToePlay class
	instanceVariableNames: ''!

!TicTacToePlay class methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 13:36:03'!
player: aPlayer at: aPosition
	^self new initializeWithPlayer: aPlayer at: aPosition.! !


!classDefinition: #TicTacToeState category: #TicTacToe!
Object subclass: #TicTacToeState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeState methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:23:32'!
putOWith: aClosureForPut checkWinWith: aClosureForCheck
	self subclassResponsibility 
! !

!TicTacToeState methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:23:34'!
putXWith: aClosureForPut checkWinWith: aClosureForCheck
	self subclassResponsibility 
! !


!TicTacToeState methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:51:29'!
OHasWon
	self subclassResponsibility 

! !

!TicTacToeState methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:52:06'!
OIsPlaying
	self subclassResponsibility 

! !

!TicTacToeState methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:51:34'!
XHasWon
	self subclassResponsibility 

! !

!TicTacToeState methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:52:04'!
XIsPlaying
	self subclassResponsibility 

! !

!TicTacToeState methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:50:41'!
isOver
	self subclassResponsibility 

! !

!TicTacToeState methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:51:51'!
isTied
	self subclassResponsibility 

! !


!classDefinition: #TicTacToeStateGameOver category: #TicTacToe!
TicTacToeState subclass: #TicTacToeStateGameOver
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateGameOver methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:23:14'!
putOWith: aClosureForPut checkWinWith: aClosureForCheck
	self error: TicTacToe canNotPlayWhenGameIsOverErrorMessage.! !

!TicTacToeStateGameOver methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:23:19'!
putXWith: aClosureForPut checkWinWith: aClosureForCheck
	self error: TicTacToe canNotPlayWhenGameIsOverErrorMessage.! !


!TicTacToeStateGameOver methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:53:58'!
OHasWon
	^ self subclassResponsibility ! !

!TicTacToeStateGameOver methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:53:46'!
OIsPlaying
	^false! !

!TicTacToeStateGameOver methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:54:00'!
XHasWon
	^ self subclassResponsibility ! !

!TicTacToeStateGameOver methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:53:43'!
XIsPlaying
	^false! !

!TicTacToeStateGameOver methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:47:02'!
isOver
	^true! !

!TicTacToeStateGameOver methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:54:05'!
isTied
	^ self subclassResponsibility ! !


!classDefinition: #TicTacToeStateOWon category: #TicTacToe!
TicTacToeStateGameOver subclass: #TicTacToeStateOWon
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateOWon methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:04'!
OHasWon
	^ true! !

!TicTacToeStateOWon methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:10'!
XHasWon
	^ false! !

!TicTacToeStateOWon methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:14'!
isTied
	^ false! !


!TicTacToeStateOWon methodsFor: 'printing' stamp: 'JOP 6/4/2018 15:04:56'!
printOn: aStream
	aStream nextPutAll: 'Juego terminado - Gan� O!!'! !


!classDefinition: #TicTacToeStateTied category: #TicTacToe!
TicTacToeStateGameOver subclass: #TicTacToeStateTied
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateTied methodsFor: 'printing' stamp: 'JOP 6/4/2018 15:06:29'!
printOn: aStream
	aStream nextPutAll: 'Juego terminado - Empataron'! !


!TicTacToeStateTied methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:39'!
OHasWon
	^ false! !

!TicTacToeStateTied methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:37'!
XHasWon
	^ false! !

!TicTacToeStateTied methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:30'!
isTied
	^ true! !


!classDefinition: #TicTacToeStateXWon category: #TicTacToe!
TicTacToeStateGameOver subclass: #TicTacToeStateXWon
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateXWon methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:46'!
OHasWon
	^ false! !

!TicTacToeStateXWon methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:51'!
XHasWon
	^ true! !

!TicTacToeStateXWon methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:55:56'!
isTied
	^ false! !


!TicTacToeStateXWon methodsFor: 'printing' stamp: 'JOP 6/4/2018 15:04:31'!
printOn: aStream
	aStream nextPutAll: 'Juego terminado - Gan� X!!'! !


!classDefinition: #TicTacToeStateTurn category: #TicTacToe!
TicTacToeState subclass: #TicTacToeStateTurn
	instanceVariableNames: 'turn'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateTurn methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:08:38'!
putOWith: aClosureForPut checkWinWith: aClosureForCheck
	^ self subclassResponsibility.! !

!TicTacToeStateTurn methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:08:44'!
putXWith: aClosureForPut checkWinWith: aClosureForCheck
	^ self subclassResponsibility.! !


!TicTacToeStateTurn methodsFor: 'initialization' stamp: 'JOP 5/31/2018 12:18:08'!
initializeWith: aTurnNumber
	turn := aTurnNumber ! !


!TicTacToeStateTurn methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:56:45'!
OHasWon
	^ false! !

!TicTacToeStateTurn methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:57:19'!
OIsPlaying
	^ self subclassResponsibility ! !

!TicTacToeStateTurn methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:56:42'!
XHasWon
	^ false! !

!TicTacToeStateTurn methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:57:18'!
XIsPlaying
	^ self subclassResponsibility ! !

!TicTacToeStateTurn methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:56:24'!
isOver
	^ false! !

!TicTacToeStateTurn methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:56:35'!
isTied
	^ false! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TicTacToeStateTurn class' category: #TicTacToe!
TicTacToeStateTurn class
	instanceVariableNames: ''!

!TicTacToeStateTurn class methodsFor: 'instance creation' stamp: 'JOP 5/31/2018 12:21:33'!
atTurn: aTurnNumber
	^self new initializeWith: aTurnNumber.! !


!classDefinition: #TicTacToeStateTurnO category: #TicTacToe!
TicTacToeStateTurn subclass: #TicTacToeStateTurnO
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateTurnO methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:22:01'!
putOWith: aClosureForPut checkWinWith: aClosureForCheck
	aClosureForPut value.
	aClosureForCheck value ifTrue: [ ^ TicTacToeStateOWon new ].
	^ TicTacToeStateTurnX atTurn: turn + 1.! !

!TicTacToeStateTurnO methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:10:15'!
putXWith: aClosureForPut checkWinWith: aClosureForCheck
	self error: TicTacToe notXTurnErrorMessage.! !


!TicTacToeStateTurnO methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:57:59'!
OIsPlaying
	^ true! !

!TicTacToeStateTurnO methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:57:39'!
XIsPlaying
	^ false! !


!TicTacToeStateTurnO methodsFor: 'printing' stamp: 'JOP 6/4/2018 15:05:43'!
printOn: aStream
	aStream nextPutAll: 'Jugando O'! !


!classDefinition: #TicTacToeStateTurnX category: #TicTacToe!
TicTacToeStateTurn subclass: #TicTacToeStateTurnX
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeStateTurnX methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:15:18'!
putOWith: aClosureForPut checkWinWith: aClosureForCheck
	self error: TicTacToe notOTurnErrorMessage.! !

!TicTacToeStateTurnX methodsFor: 'playing' stamp: 'JOP 5/31/2018 12:29:10'!
putXWith: aClosureForPut checkWinWith: aClosureForCheck
	aClosureForPut value.
	aClosureForCheck value ifTrue: [ ^ TicTacToeStateXWon new ].
	turn = 5 ifTrue: [ ^ TicTacToeStateTied new ].
	^ TicTacToeStateTurnO atTurn: turn.! !


!TicTacToeStateTurnX methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:57:53'!
OIsPlaying
	^ false! !

!TicTacToeStateTurnX methodsFor: 'testing' stamp: 'JOP 5/31/2018 04:57:48'!
XIsPlaying
	^ true! !


!TicTacToeStateTurnX methodsFor: 'printing' stamp: 'JOP 6/4/2018 15:06:06'!
printOn: aStream
	aStream nextPutAll: 'Jugando X'! !


!classDefinition: #TicTacToeViewerHub category: #TicTacToe!
Object subclass: #TicTacToeViewerHub
	instanceVariableNames: 'tictactoe viewers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToe'!

!TicTacToeViewerHub methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 14:44:16'!
attach: aViewer
	viewers add: aViewer.! !

!TicTacToeViewerHub methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 14:50:17'!
initializeViewing: aTicTacToe
	tictactoe _ aTicTacToe.
	viewers _ OrderedCollection new.
	tictactoe onPlay: [:play :state | viewers do: [:viewer | viewer notify: play onState: state]].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TicTacToeViewerHub class' category: #TicTacToe!
TicTacToeViewerHub class
	instanceVariableNames: ''!

!TicTacToeViewerHub class methodsFor: 'as yet unclassified' stamp: 'JOP 6/4/2018 14:44:56'!
view: aTicTacToe
	^ self new initializeViewing: aTicTacToe.! !
